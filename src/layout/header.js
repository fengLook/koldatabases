import React from 'react';
import { Layout,Menu, Modal, Icon, Divider, Form, Input, Dropdown, Row, Col, Avatar } from 'antd';
import Link from 'umi/link';
const { Header } = Layout;
import styles from './index.less';

const FormItem = Form.Item;// 引入表单组件
const formItemLayout = {
    labelCol: {
        xs: { span: 18 },
        sm: { span: 6 },
    },
    wrapperCol: {
        xs: { span: 6 },
        sm: { span: 16 },
    },
};

class IndexHeader extends React.Component {
    state = { visible: false }
    showModal = () => {
        console.log("eee");
        this.setState({
            visible: true,
        });
    }
    handleOk = (e) => {
        this.setState({
            visible: false,
        });
    }
    handleCancel = (e) => {
        console.log(e);
        this.setState({
            visible: false,
        });
    }
    // 个人图像下 子菜单内容
    headerSonMenu = (
        <Menu>
            <Menu.Item>
                <a target="_blank" rel="noopener noreferrer" onClick={this.showModal}>
                    <Icon type="setting" />
                    &nbsp;修改密码
                </a>
            </Menu.Item>
            <Menu.Item>
                <a target="_blank" rel="noopener noreferrer" href="javascript:void (0)">
                    <Icon type="logout" />
                    &nbsp;退出
                </a>
            </Menu.Item>
        </Menu>
    );
    render() {
        return (
            <Header className={styles.watsonKOLHeader}>
                <Row justify="space-between" style={{ color: '#fff' }}>
                <Col span={12}>
                    <div style={{ fontWeight: '700', fontSize: '18px' }}>
                        <Link to="/">
                            <img src="images/logo.png" style={{ width: '44px', height: '44px' }} />
                        </Link>
                        <Divider type="vertical" />Watsons KOL
                    </div>
                </Col>
                    <Col span={12} style={{ textAlign: 'right' }}>
                        <div>
                            <span style={{
                                    cursor: 'pointer',
                                    padding: '0 12px',
                                    display: 'inline-block',
                                    height: '100%',
                                    transition: 'all .3s',
                                    position: 'relative',
                                }}>
                            <span style={{
                                  height: '18px',
                                  background: 'rgb(245, 34, 45)',
                                  padding: '0 12px',
                                  color: 'rgb(255, 255, 255)',
                                  textAlign: 'center',
                                  position: 'absolute',
                                  lineHeight: '18px',
                                  fontSize: '12px',
                                  borderRadius: '20px',
                                  top: '12px',
                              }}>
                            12
                          </span>
                          <Icon
                              type="bell"
                              theme="outlined"
                              style={{ fontSize: '18px', verticalAlign: '-0.24em' }}
                          />
                        </span>
                            <Dropdown overlay={this.headerSonMenu} className={styles.colorWhite}>
                                <a className="ant-dropdown-link" href="javascript:void(0)">
                          <span
                              style={{
                                  cursor: 'pointer',
                                  padding: '0 12px',
                                  paddingRight: '5px',
                                  display: 'inline-block',
                                  height: '100%',
                                  transition: 'all .3s',
                              }}
                          >
                            <Avatar style={{ color: '#f56a00', backgroundColor: '#fde3cf', verticalAlign: 'middle' }} size="small" >U</Avatar>
                          </span>
                                    {this.props.name} <Icon type="down" />
                                </a>
                            </Dropdown>
                        </div>
                        <Modal
                            title="修改密码"
                            visible={this.state.visible}
                            onOk={this.handleOk}
                            onCancel={this.handleCancel}
                        >
                            <Form>
                                <FormItem {...formItemLayout} label="旧密码">
                                    <Input placeholder="请输入旧密码" />
                                </FormItem>
                                <FormItem {...formItemLayout} label="新密码">
                                    <Input placeholder="请输入新密码" />
                                </FormItem>
                                <FormItem {...formItemLayout} label="确认新密码">
                                    <Input placeholder="请再次输入新密码" />
                                </FormItem>
                            </Form>
                        </Modal>
                    </Col>
                </Row>
            </Header>
        );
    }
}

export default IndexHeader;
