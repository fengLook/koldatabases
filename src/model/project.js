import { Button, Divider } from 'antd';
import Link from 'umi/link';
export default {
    namespace: 'project',
    state: {
        data:[
            {
                key: '1',
                code: 'HO20180925475047',
                name: '1115selling kit',
                startTime: '2018/11/15',
                region: '南区',
                status: '正常',
                championReview: '已审核',
                hoReview: '已审核',
                reason: '不通过原因',
            },
            {
                key: '2',
                code: 'HO20180914102810',
                name: '1218power pack',
                startTime: '2018/11/15',
                region: '北区',
                status: '正常',
                championReview: '已审核',
                hoReview: '已审核',
                reason: '不通过原因',
            },
            {
                key: '3',
                code: 'HO2018091408368',
                name: '1115NEWEB',
                startTime: '2018/11/15',
                region: '南区',
                status: '正常',
                championReview: '已审核',
                hoReview: '已审核',
                reason: '不通过原因',
            },
            {
                key: '4',
                code: 'HO20180913121712',
                name: '1115power pack',
                startTime: '2018/11/15',
                region: '西区',
                status: '正常',
                championReview: '已审核',
                hoReview: '已审核',
                reason: '不通过原因',
            },
        ]
    }
}