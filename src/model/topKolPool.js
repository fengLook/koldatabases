import { Button, Divider } from 'antd';
import Link from 'umi/link';
import { Rate, Icon } from 'antd';

export default {
  namespace: 'topKolPool',
  state: {
      kolColumnsPPBG: [
          {
              title: '平台',
              dataIndex: 'platform',
          },
          {
              title: '账号',
              dataIndex: 'name',
              render: text => <Link to="/kol/detail/anyParams">{text}</Link>,
          },
          {
              title: '合作费用（最新）',
              dataIndex: 'cost',
          },
          // {
          //     title: '阅读量',
          //     dataIndex: 'readership',
          // },
          {
              title: '前测数据（最新）',
              dataIndex: 'pretestData',
              // render:  (text,showQianCeModal) => {
              //     if(text>=30){
              //         return <span><a href="javascript:void(0);" onClick={showQianCeModal}>{text}</a><Icon type="caret-up" style={{color:'#32CD32'}}/></span>
              //     }else  if(text>=0){
              //         return <span><a href="javascript:void(0);" onClick={showQianCeModal}>{text}</a><Icon type="caret-down" style={{color:'#f00'}} /></span>
              //     }else {
              //         return <span><a href="javascript:void(0);" onClick={showQianCeModal}>{text}</a></span>
              //     }
              // },
          },
          {
              title: '后测数据（最新）',
              dataIndex: 'postTestData',
          },
          {
              title: '粉丝数',
              dataIndex: 'fansCount',
          },
          {
              title: '有效粉丝率',
              dataIndex: 'effectiveFanRate',
          },
          {
              title: '有效粉丝数',
              dataIndex: 'effectiveFanCount',
          },
          {
              title: 'CPR平均数',
              dataIndex: 'CPR',
          },
          {
              title: '星级',
              key: 'starLevel',
              dataIndex: 'starLevel',
              width: 200,
              render: start => {
                  return <Rate disabled defaultValue={start}/>;
              },
          },
      ],
      dataPPBG: [
          {
              key: '4',
              platform: '微博',
              name: '靖一Jenn',
              cost: '5200',
              pretestData: '52',
              postTestData: '55',
              fansCount: '1161271',//粉丝数
              effectiveFanRate: '12.41%',//有效粉丝率
              effectiveFanCount: '139352',//有效粉丝数
              readership: 2802560,
              CPR: '0.0018',
              starLevel: 5
          },
          {
              key: '5',
              platform: '微博',
              name: 'Jo_Makeup',
              cost: '5000',
              pretestData: '55',
              postTestData: '53',
              fansCount: '530676',//粉丝数
              effectiveFanRate: '7.89%',//有效粉丝率
              effectiveFanCount: '41870',//有效粉丝数
              readership: 2862560,
              CPR: '0.0017',
              starLevel: 5
          },
          {
              key: '6',
              platform: '微博',
              name: 'Jamie_baby',
              cost: '5800',
              pretestData: '58',
              postTestData: '53',
              fansCount: '1092180',//粉丝数
              effectiveFanRate: '14.4%',//有效粉丝率
              effectiveFanCount: '139352',//有效粉丝数
              readership: 3262560,
              CPR: '0.0017',
              starLevel: 5
          },
          {
              key: '7',
              platform: '微博',
              name: '亦南Astrid',
              cost: '6000',
              pretestData: '51',
              postTestData: '52',
              fansCount: '930245',//粉丝数
              effectiveFanRate: '12.41%',//有效粉丝率
              effectiveFanCount: '111629',//有效粉丝数
              readership: 3862560,
              CPR: '0.0015',
              starLevel: 5
          },
          {
              key: '10',
              platform: '微博',
              name: '柳雪冷',
              cost: '5500',
              pretestData: '48',
              postTestData: '49',
              fansCount: '751877',//粉丝数
              effectiveFanRate: '8.23%',//有效粉丝率
              effectiveFanCount: '61879',//有效粉丝数
              readership: 3362560,
              CPR: '0.0016',
              starLevel: 5
          },
          {
              key: '9',
              platform: '微博',
              name: 'aki猫咪',
              cost: '6000',
              pretestData: '55',
              postTestData: '50',
              fansCount: '3249380',//粉丝数
              effectiveFanRate: '10%',//有效粉丝率
              effectiveFanCount: '324938',//有效粉丝数
              readership: 3662560,
              CPR: '0.0016',
              starLevel: 5
          },
          {
              key: '8',
              platform: '微博',
              name: 'C宝CC',
              cost: '6200',
              pretestData: '58',
              postTestData: '52',
              fansCount: '1211694',//粉丝数
              effectiveFanRate: '2.83%',//有效粉丝率
              effectiveFanCount: '33927',//有效粉丝数
              readership: 2862560,
              CPR: '0.0021',
              starLevel: 3
          },
          {
              key: '3',
              platform: '微博',
              name: '三三子Coco',
              cost: '6300',
              pretestData: '63',
              postTestData: '60',
              fansCount: '1092941',//粉丝数
              effectiveFanRate: '16.42%',//有效粉丝率
              effectiveFanCount: '17924232',//有效粉丝数
              readership: 3002560,
              CPR: '0.002',
              starLevel: 3
          },
          {
              key: '1',
              platform: '微博',
              name: '于艾希Icey',
              cost: '6800',
              readership: 1825600,
              pretestData: '66',
              postTestData: '67',
              fansCount: '1161271',//粉丝数
              effectiveFanRate: '12.41%',//有效粉丝率
              effectiveFanCount: '139352',//有效粉丝数
              CPR: '0.037',
              starLevel: 1
          },
          {
              key: '2',
              platform: '微博',
              name: '小李丽莎V',
              cost: '6500',
              readership: 1802560,
              CPR: '0.036',
              starLevel: 1,
              pretestData: '68',
              postTestData: '66',
              fansCount: '572800',//粉丝数
              effectiveFanRate: '18.41%',//有效粉丝率
              effectiveFanCount: '105452',//有效粉丝数
          },
      ],
      kolColumnsZC: [
          {
              title: '平台',
              dataIndex: 'platform',
          },
          {
              title: '账号',
              dataIndex: 'name',
              render: text => <Link to="/kol/detail/anyParams">{text}</Link>,
          },
          {
              title: '合作费用（最新）',
              dataIndex: 'cooperationFee',
          },
          // {
          //     title: '点击',
          //     dataIndex: 'numberOfClick',
          // },
          {
              title: '前测数据（最新）',
              dataIndex: 'pretestData',
          },
          {
              title: '后测数据（最新）',
              dataIndex: 'postTestData',
          },
          {
              title: '粉丝数',
              dataIndex: 'fansCount',
          },
          {
              title: '有效粉丝率',
              dataIndex: 'effectiveFanRate',
          },
          {
              title: '有效粉丝数',
              dataIndex: 'effectiveFanCount',
          },
          {
              title: 'CPC平均数',
              dataIndex: 'CPC',
          },
          {
              title: '星级',
              key: 'starLevel',
              dataIndex: 'starLevel',
              width: 200,
              render: start => {
                  return <Rate disabled defaultValue={start}/>;
              },
          },
      ],
      dataZC: [
          {
              key: '1',
              platform: '微博',
              name: '于艾希Icey',
              cooperationFee: '6800',
              pretestData: '66',
              postTestData: '67',
              fansCount: '1161271',//粉丝数
              effectiveFanRate: '12.41%',//有效粉丝率
              effectiveFanCount: '139352',//有效粉丝数
              numberOfClick:360,
              CPC: '18.8',
              starLevel: 5
          },
          {
              key: '2',
              platform: '微博',
              name: '小李丽莎V',
              cooperationFee: '6500',
              pretestData: '68',
              postTestData: '66',
              fansCount: '572800',//粉丝数
              effectiveFanRate: '18.41%',//有效粉丝率
              effectiveFanCount: '105452',//有效粉丝数
              numberOfClick:385,
              CPC: '16.8',
              starLevel: 5
          },
          {
              key: '3',
              platform: '微博',
              name: '三三子Coco',
              cooperationFee: '6300',
              pretestData: '63',
              postTestData: '60',
              fansCount: '1092941',//粉丝数
              effectiveFanRate: '16.42%',//有效粉丝率
              effectiveFanCount: '17924232',//有效粉丝数
              numberOfClick:250,
              CPC: '25.2',
              starLevel: 5
          },
          {
              key: '4',
              platform: '微博',
              name: '靖一Jenn',
              cooperationFee: '5200',
              pretestData: '52',
              postTestData: '55',
              fansCount: '1161271',//粉丝数
              effectiveFanRate: '12.41%',//有效粉丝率
              effectiveFanCount: '139352',//有效粉丝数
              numberOfClick:150,
              CPC: '34.7',
              starLevel: 3
          },
          {
              key: '5',
              platform: '微博',
              name: 'Jo_Makeup',
              cooperationFee: '5000',
              pretestData: '55',
              postTestData: '53',
              fansCount: '530676',//粉丝数
              effectiveFanRate: '7.89%',//有效粉丝率
              effectiveFanCount: '41870',//有效粉丝数
              numberOfClick:150,
              CPC: '33.3',
              starLevel: 3
          },
          {
              key: '6',
              platform: '微博',
              name: 'Jamie_baby',
              cooperationFee: '5800',
              pretestData: '58',
              postTestData: '53',
              fansCount: '1092180',//粉丝数
              effectiveFanRate: '14.4%',//有效粉丝率
              effectiveFanCount: '139352',//有效粉丝数
              numberOfClick:150,
              CPC: '38.6',
              starLevel: 3
          },
          {
              key: '7',
              platform: '微博',
              name: '亦南Astrid',
              cooperationFee: '6000',
              pretestData: '51',
              postTestData: '52',
              fansCount: '930245',//粉丝数
              effectiveFanRate: '12.41%',//有效粉丝率
              effectiveFanCount: '111629',//有效粉丝数
              numberOfClick:148,
              CPC: '40.5',
              starLevel: 1
          },
          {
              key: '8',
              platform: '微博',
              name: 'C宝CC',
              cooperationFee: '6200',
              pretestData: '58',
              postTestData: '52',
              fansCount: '1211694',//粉丝数
              effectiveFanRate: '2.83%',//有效粉丝率
              effectiveFanCount: '33927',//有效粉丝数
              numberOfClick:140,
              CPC: '44.3',
              starLevel: 1
          },
          {
              key: '9',
              platform: '微博',
              name: 'aki猫咪',
              cooperationFee: '6000',
              pretestData: '55',
              postTestData: '50',
              fansCount: '3249380',//粉丝数
              effectiveFanRate: '10%',//有效粉丝率
              effectiveFanCount: '324938',//有效粉丝数
              numberOfClick:130,
              CPC: '46.2',
              starLevel: 1
          },
          {
              key: '10',
              platform: '微博',
              name: '柳雪冷',
              cooperationFee: '5500',
              pretestData: '48',
              postTestData: '49',
              fansCount: '751877',//粉丝数
              effectiveFanRate: '8.23%',//有效粉丝率
              effectiveFanCount: '61879',//有效粉丝数
              numberOfClick:120,
              CPC: '45.8',
              starLevel: 1
          },
      ],
      kolColumnsDH: [
          {
              title: '平台',
              dataIndex: 'platform',
          },
          {
              title: '账号',
              dataIndex: 'name',
              render: text => <Link to="/kol/detail/anyParams">{text}</Link>,
          },
          {
              title: '合作费用（最新）',
              dataIndex: 'cooperationFee',
          },
          {
              title: '前测数据（最新）',
              dataIndex: 'pretestData',
          },
          {
              title: '后测数据（最新）',
              dataIndex: 'postTestData',
          },
          {
              title: '粉丝数',
              dataIndex: 'fansCount',
          },
          {
              title: '有效粉丝率',
              dataIndex: 'effectiveFanRate',
          },
          {
              title: '有效粉丝数',
              dataIndex: 'effectiveFanCount',
          },
          // {
          //     title: '平均ROI',
          //     dataIndex: 'averageROI',
          // },
          {
              title: '星级',
              key: 'starLevel',
              dataIndex: 'starLevel',
              width: 200,
              render: start => {
                  return <Rate disabled defaultValue={start}/>;
              },
          },
      ],
      dataDH: [
          {
              key: '1',
              platform: '微博',
              name: '于艾希Icey',
              cooperationFee: '6800',
              pretestData: '66',
              postTestData: '67',
              fansCount: '1161271',//粉丝数
              effectiveFanRate: '12.41%',//有效粉丝率
              effectiveFanCount: '139352',//有效粉丝数
              averageROI: '0.91',
              starLevel: 5
          },
          {
              key: '2',
              platform: '微博',
              name: '小李丽莎V',
              cooperationFee: '6500',
              pretestData: '68',
              postTestData: '66',
              fansCount: '572800',//粉丝数
              effectiveFanRate: '18.41%',//有效粉丝率
              effectiveFanCount: '105452',//有效粉丝数
              averageROI: '0.88',
              starLevel: 5
          },
          {
              key: '3',
              platform: '微博',
              name: '三三子Coco',
              cooperationFee: '6300',
              pretestData: '63',
              postTestData: '60',
              fansCount: '1092941',//粉丝数
              effectiveFanRate: '16.42%',//有效粉丝率
              effectiveFanCount: '17924232',//有效粉丝数
              averageROI: '0.79',
              starLevel: 5
          },
          {
              key: '4',
              platform: '微博',
              name: '靖一Jenn',
              cooperationFee: '5200',
              pretestData: '52',
              postTestData: '55',
              fansCount: '1161271',//粉丝数
              effectiveFanRate: '12.41%',//有效粉丝率
              effectiveFanCount: '139352',//有效粉丝数
              averageROI: '0.67',
              starLevel: 5
          },
          {
              key: '5',
              platform: '微博',
              name: 'Jo_Makeup',
              cooperationFee: '5000',
              pretestData: '55',
              postTestData: '53',
              fansCount: '530676',//粉丝数
              effectiveFanRate: '7.89%',//有效粉丝率
              effectiveFanCount: '41870',//有效粉丝数
              averageROI: '0.66',
              starLevel: 3
          },
          {
              key: '6',
              platform: '微博',
              name: 'Jamie_baby',
              cooperationFee: '5800',
              pretestData: '58',
              postTestData: '53',
              fansCount: '1092180',//粉丝数
              effectiveFanRate: '14.4%',//有效粉丝率
              effectiveFanCount: '139352',//有效粉丝数
              averageROI: '0.77',
              starLevel: 3
          },
          {
              key: '7',
              platform: '微博',
              name: '亦南Astrid',
              cooperationFee: '6000',
              pretestData: '51',
              postTestData: '52',
              fansCount: '930245',//粉丝数
              effectiveFanRate: '12.41%',//有效粉丝率
              effectiveFanCount: '111629',//有效粉丝数
              averageROI: '0.75',
              starLevel: 3
          },
          {
              key: '8',
              platform: '微博',
              name: 'C宝CC',
              cooperationFee: '6200',
              pretestData: '58',
              postTestData: '52',
              fansCount: '1211694',//粉丝数
              effectiveFanRate: '2.83%',//有效粉丝率
              effectiveFanCount: '33927',//有效粉丝数
              averageROI: '0.56',
              starLevel: 1
          },
          {
              key: '9',
              platform: '微博',
              name: 'aki猫咪',
              cooperationFee: '6000',
              pretestData: '55',
              postTestData: '50',
              fansCount: '3249380',//粉丝数
              effectiveFanRate: '10%',//有效粉丝率
              effectiveFanCount: '324938',//有效粉丝数
              averageROI: '0.67',
              starLevel: 1
          },
          {
              key: '10',
              platform: '微博',
              name: '柳雪冷',
              cooperationFee: '5500',
              pretestData: '48',
              postTestData: '49',
              fansCount: '751877',//粉丝数
              effectiveFanRate: '8.23%',//有效粉丝率
              effectiveFanCount: '61879',//有效粉丝数
              averageROI: '0.76',
              starLevel: 1
          },
      ],
  },
};

