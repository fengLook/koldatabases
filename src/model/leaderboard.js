import { Button, Divider } from 'antd';
import Link from 'umi/link';
import { Rate, Icon } from 'antd';

export default {
  namespace: 'leaderboard',
  state: {
      leaderboardColumns : [
          {
              title: '排名',
              dataIndex: 'ranking',
              key: 'ranking',
          },{
              title: 'Name',
              dataIndex: 'name',
              key: 'name',
          }, {
              title:'KII',
              dataIndex: 'KII',
              key: 'KII',
          },
          {
              title: '星级',
              key: 'starLevel',
              dataIndex: 'starLevel',
              width: 200,
              render: start => {
                  return <Rate disabled defaultValue={start}/>;
              },
          },
      ],
      weiboData : [
          {
              key: '1',
              ranking:'1',
              name: '于艾希Icey',
              starLevel: 5,
              KII: 67,
          }, {
              key: '2',
              ranking:'2',
              name: '小李丽莎V',
              starLevel: 5,
              KII: 66,
          }, {
              key: '3',
              ranking:'3',
              name: '三三子Coco',
              starLevel: 5,
              KII: 60,
          }, {
              key: '4',
              ranking:'4',
              name: '靖一Jenn',
              starLevel: 5,
              KII: 55,
          }, {
              key: '5',
              ranking:'5',
              name: 'Jo_Makeup',
              starLevel: 5,
              KII: 53,
          }, {
              key: '6',
              ranking:'6',
              name: 'Jamie_baby',
              starLevel: 5,
              KII: 53,
          }, {
              key: '7',
              ranking:'7',
              name: '亦南Astrid',
              starLevel: 5,
              KII: 52,
          }, {
              key: '8',
              ranking:'8',
              name: 'C宝CC',
              starLevel: 5,
              KII: 52,
          }, {
              key: '9',
              ranking:'9',
              name: 'aki猫咪',
              starLevel: 5,
              KII: 50,
          }, {
              key: '10',
              ranking:'10',
              name: '柳雪冷',
              starLevel: 5,
              KII: 49,
          }],
      wexinData : [
          {
              key: '1',
              ranking:'1',
              name: 'TomatoP番茄',
              starLevel: 5,
              KII: 92,
          }, {
              key: '2',
              ranking:'2',
              name: '时尚生活派',
              starLevel: 5,
              KII: 68,
          }, {
              key: '3',
              ranking:'3',
              name: 'aki猫咪',
              starLevel: 5,
              KII: 91,
          }, {
              key: '4',
              ranking:'4',
              name: '妖精边儿',
              starLevel: 5,
              KII: 90,
          }, {
              key: '5',
              ranking:'5',
              name: 'JK珺的时光手记',
              starLevel: 5,
              KII: 89,
          }, {
              key: '6',
              ranking:'6',
              name: '手边巴黎',
              starLevel: 5,
              KII: 89,
          }, {
              key: '7',
              ranking:'7',
              name: 'YOKA生活',
              starLevel: 5,
              KII: 87,
          }, {
              key: '8',
              ranking:'8',
              name: '生活Gogoing',
              starLevel: 5,
              KII: 87,
          }, {
              key: '9',
              ranking:'9',
              name: '小树幸福',
              starLevel: 5,
              KII: 84,
          }, {
              key: '10',
              ranking:'10',
              name: '黎贝卡的异想世界',
              starLevel: 5,
              KII: 83,
          }],
      xssData :  [
          {
              key: '1',
              ranking:'1',
              name: '马哈哈哈哈哈哈哈哈哈哈哈',
              starLevel: 5,
              KII: 68,
          }, {
              key: '2',
              ranking:'2',
              name: '柠檬Suki',
              starLevel: 5,
              KII: 67,
          }, {
              key: '3',
              ranking:'3',
              name: 'belle小米',
              starLevel: 5,
              KII: 64,
          }, {
              key: '4',
              ranking:'4',
              name: 'Wu_xiaoxu',
              starLevel: 5,
              KII: 64,
          }, {
              key: '5',
              ranking:'5',
              name: 'Jim Red',
              starLevel: 5,
              KII: 49,
          }, {
              key: '6',
              ranking:'6',
              name: '花小拉',
              starLevel: 5,
              KII: 61,
          }, {
              key: '7',
              ranking:'7',
              name: 'killiancle',
              starLevel: 5,
              KII: 58,
          }, {
              key: '8',
              ranking:'8',
              name: '叁月是蒜瓣儿',
              starLevel: 5,
              KII: 58,
          }, {
              key: '9',
              ranking:'9',
              name: 'MissCharming',
              starLevel: 5,
              KII: 57,
          }, {
              key: '10',
              ranking:'10',
              name: '是维尼耶',
              starLevel: 5,
              KII: 57,
          }],
  },
};
