import { Button, Divider } from 'antd';
import Link from 'umi/link';

export default {
  namespace: 'kols',
  state: {
      isAgree: true,
      columns: [
        {
          title: '账户名',
          dataIndex: 'name',
          render: text => <Link to="/kol/detail/anyParams">{text}</Link>,
        },
        {
          title: '平台',
          dataIndex: 'platform',
        },
        {
          title: '区域',
          dataIndex: 'region',
        },
        {
          title: '所在地',
          dataIndex: 'local',
        },
        {
          title: '黑名单',
          dataIndex: 'isBlacklist',
        },
        {
          title: '区域审核',
          dataIndex: 'regionReview',
        },
        {
          title: '总部审核',
          dataIndex: 'headquartersReview',
        },
        {
          title: '财务审核',
          dataIndex: 'financeReview',
        },
        {
          title: '是否确定条款',
          dataIndex: 'isConfirm',
        },
        {
          title: '操作',
          key: 'action',
          render: (text, record) => (
            <div> <Button type="primary">区域评分</Button>
              <Divider type="vertical" />
              <Button type="primary">总部评分</Button>
              <Divider type="vertical" />
              <Button type="danger">拉黑</Button>
            </div>
          ),
        },
      ],
      kolList: [
        {
            key: '1',
            name: '帅你一脸毛蛋',
            phone: '--',
            platform: '微博', //平台
            region: '华东', //参与区域
            local: '山东',
            isBlacklist: '否', //是否黑名单
            regionReview: '已审核', //区域审核
            headquartersReview: '已审核', //总部审核情况
            financeReview: '已审核', //财务审核
            address: '山东 济南', //地址
            isConfirm: '是', //是否确定条款
            cooperationFee: 1500,//合作费用
            fansCount: 2033569, //粉丝数
            platformName: '帅你一脸毛蛋', //平台账号名
            averageReading: 4892, //平均阅读量
            averageInteractionNumber: 4892,//平均互动量
            pastCooperationCosts: '--' //过往合作费用

        },
        {
            key: '2',
            name: '扇子NO_FAN_NO_FUN',
            phone: '--',
            platform: '微博',
            region: '海外',
            local: '海外 其他',
            isBlacklist: '否',
            regionReview: '已审核',
            headquartersReview: '已审核',
            financeReview: '已审核',
            address: '海外 其他',
            isConfirm: '否',
            cooperationFee: 2500,
            fansCount:  1092941,
            platformName: '扇子NO_FAN_NO_FUN',
            averageReading: 1500,
            averageInteractionNumber: 1500,
            pastCooperationCosts: '--'
        },
        {
            key: '3',
            name: '家弘',
            phone: '--',
            platform: '微博',
            region: '华东',
            local: '上海',
            isBlacklist: '否',
            regionReview: '已审核',
            headquartersReview: '已审核',
            financeReview: '已审核',
            address: '上海 静安区',
            isConfirm: '是',
            cooperationFee: 1500,
            fansCount: 1121928,
            platformName: '家弘',
            averageReading: 500,
            averageInteractionNumber: 1500,
            pastCooperationCosts:'--'
        },
          {
              key: '4',
              name: '王丹妮Dannie',
              phone: '--',
              platform: '微信', //平台
              region: '--', //参与区域
              local: '--',
              isBlacklist: '否', //是否黑名单
              regionReview: '已审核', //区域审核
              headquartersReview: '已审核', //总部审核情况
              financeReview: '已审核', //财务审核
              address: '--', //地址
              isConfirm: '是', //是否确定条款
              cooperationFee: 1500,//合作费用
              fansCount: 2033569, //粉丝数
              platformName: '王丹妮Dannie', //平台账号名
              averageReading: 4892, //平均阅读量
              averageInteractionNumber: 4892,//平均互动量
              pastCooperationCosts: '--' //过往合作费用

          },
          {
              key: '5',
              name: '手边巴黎',
              phone: '--',
              platform: '微信', //平台
              region: '--', //参与区域
              local: '--',
              isBlacklist: '否', //是否黑名单
              regionReview: '已审核', //区域审核
              headquartersReview: '已审核', //总部审核情况
              financeReview: '已审核', //财务审核
              address: '--', //地址
              isConfirm: '是', //是否确定条款
              cooperationFee: 1500,//合作费用
              fansCount: 2033569, //粉丝数
              platformName: '王丹妮Dannie', //平台账号名
              averageReading: 100001, //平均阅读量
              averageInteractionNumber: 100001,//平均互动量
              pastCooperationCosts: '--' //过往合作费用

          },
          {
              key: '6',
              name: '新氧',
              phone: '--',
              platform: '微信', //平台
              region: '--', //参与区域
              local: '--',
              isBlacklist: '否', //是否黑名单
              regionReview: '已审核', //区域审核
              headquartersReview: '已审核', //总部审核情况
              financeReview: '已审核', //财务审核
              address: '--', //地址
              isConfirm: '是', //是否确定条款
              cooperationFee: 1500,//合作费用
              fansCount: 2033569, //粉丝数
              platformName: '王丹妮Dannie', //平台账号名
              averageReading: 4892, //平均阅读量
              averageInteractionNumber: 4892,//平均互动量
              pastCooperationCosts: '--' //过往合作费用

          },

      ],
      popAddKolColumns: [
          {
              title: '姓名',
              dataIndex: 'name',
              render: text => <Link to="/kol/detail/anyParams">{text}</Link>,
          },
          {
              title: '合作平台',
              dataIndex: 'platform',
          },
          {
              title: '合作费用（最新）',
              dataIndex: 'cooperationFee',
          },

          {
              title: '粉丝数',
              dataIndex: 'fansCount',
          },
          {
              title: '平台账户名',
              dataIndex: 'platformName',
          },
          { title: '平均阅读量', dataIndex: 'averageReading', key: 'averageReading' },
          { title:'平均互动量', dataIndex: 'averageInteractionNumber', key:'averageInteractionNumber'},
          {
              title: '区域',
              dataIndex: 'region',
              key:'region'
          },
          {
              title: '所在地',
              dataIndex: 'local',
          },
          { title: '过往合作费用', dataIndex: 'pastCooperationCosts', key: 'pastCooperationCosts'},
          ]
  },
};
