import { routerRedux } from 'dva/router';
import { stringify } from 'qs';
import { fakeAccountLogin,checkUser, getFakeCaptcha } from '@/services/api';
import { setAuthority } from '@/utils/authority';
import { getPageQuery } from '@/utils/utils';
import { reloadAuthorized } from '@/utils/Authorized';
import $ from 'jquery';
import fetch from 'dva/fetch';

import request, {post} from '../utils/kolRequest';

export default {
  namespace: 'login',
  state: {
    status: undefined,
  },
  effects: {
    *login({ payload }, { call, put }) {

     const response = yield call(fakeAccountLogin, payload);

    //  const response = yield call(checkUser, payload);

                //
                // $.ajax({
                //   //  url : "http://10.82.33.118:3001/parse/functions/login",
                //     //url : "http://10.82.33.118:3001/parse",
                //     url : "http://localhost:1337/parse",
                //     type : "POST",
                //     contentType: "application/json;charset=utf-8",
                //   //  data : JSON.stringify({'name':name,'passwd':passwd}),
                //     data:JSON.stringify(payload),
                //     dataType : "text",
                //     success : function(result) {
                //         debugger;
                //     },
                //     error:function(msg){
                //      debugger;
                //     }
                // })


        // const response =  fetch('http://localhost:1337/parse/functions/login',{
        //         method: 'post',
        //         headers: {
        //             'Content-Type': 'application/json',

        //             'Access-Control-Allow-Origin': '*'
        //         },
        //         body: payload
        //     })
        //         .then(data => {
        //             return data;
        //         })
        //         .catch(error => console.log('error is', error));
      //  end

      yield put({
        type: 'changeLoginStatus',
        payload: response,
      });
      // Login successfully
      if (response.status === 'ok') {
          debugger;
        reloadAuthorized();
        // setAuthority
        const urlParams = new URL(window.location.href);
        const params = getPageQuery();
        let { redirect } = params;
        if (redirect) {
          const redirectUrlParams = new URL(redirect);
          if (redirectUrlParams.origin === urlParams.origin) {
            redirect = redirect.substr(urlParams.origin.length);
            if (redirect.startsWith('/#')) {
              redirect = redirect.substr(2);
            }
          } else {
            window.location.href = redirect;
            return;
         }
        }
        yield put(routerRedux.replace(redirect || '/'));
      }else {
        console.log(response);
      }
    },

    *getCaptcha({ payload }, { call }) {
      yield call(getFakeCaptcha, payload);
    },

    *logout(_, { put }) {
      yield put({
        type: 'changeLoginStatus',
        payload: {
          status: false,
          currentAuthority: 'guest',
        },
      });
      reloadAuthorized();
      yield put(
        routerRedux.push({
          pathname: '/user/login',
          search: stringify({
            redirect: window.location.href,
          }),
        })
      );
    },
  },

  reducers: {
    changeLoginStatus(state, { payload }) {
      setAuthority(payload.currentAuthority);
      return {
        ...state,
        status: payload.status,
        type: payload.type,
      };
    },
  },
};
