import { Button, Divider } from 'antd';
import Link from 'umi/link';
import { Rate, Icon } from 'antd';

export default {
    namespace: 'planDetail',
    state: {
        planDetailColumns:[
            {
                title: '编号',
                dataIndex: 'id',
            },{
                title: '所属区域',
                dataIndex: 'area',
            },{
                title: '所在城市',
                dataIndex: 'city',
            },
            {
                title: '平台',
                dataIndex: 'platform',
            },{
                title: '账户名',
                dataIndex: 'platformName',
            },
            {
                title: '账户ID或主页链接',
                dataIndex: 'Links',
            },
            {
                title: 'KOL tag',
                dataIndex: 'kolTag',
            },{
                title: '前测KII',
                dataIndex: 'pretestKII',
            },
            { title:'粉丝数', dataIndex: 'fansCount', key:'fansCount'},
            {title: '有效粉丝率', dataIndex: 'effectiveFanRate', key:'effectiveFanRate'},
            {title: '有效粉丝数', dataIndex: 'effectiveFanCount', key:'effectiveFanCount'},
            { title:'级别', dataIndex: 'level', key:'level'},
            {title: '合作费用', dataIndex: 'cooperationFee', key:'cooperationFee'},
            {title: '预估个税', dataIndex: 'estimatedTax', key:'estimatedTax'},
            { title: '过往合作费用', dataIndex: 'pastCooperationCosts', key: 'pastCooperationCosts'},
            {title: '最近帖子链接（微信）', dataIndex: 'recentPostLinkWX', key: 'recentPostLinkWX' },
            { title: '平均阅读量', dataIndex: 'averageReading', key: 'averageReading' },
            { title:'与屈臣氏上次合作阅读量', dataIndex: 'lastReadingWithWatsons', key:'lastReadingWithWatsons'},
            { title:'平均互动数', dataIndex: 'averageInteractionNumber', key:'averageInteractionNumber'},
            { title:'与屈臣氏上次合作互动数', dataIndex: 'lastInteractionWithWatsons', key:'lastInteractionWithWatsons'},
            {title: '预估阅读数', dataIndex: 'estimatedReadings', key: 'estimatedReadings'},
            {title: '预估互动数', dataIndex: 'estimatedInteractionNumber', key: 'estimatedInteractionNumber'},
            {title: 'CPR（最新）',dataIndex: 'CPR', key: 'CPR' },
            {title: 'CPC（最新）', dataIndex: 'CPC', key: 'CPC' },
            {title: 'ROI（最新）', dataIndex: 'ROI', key: 'ROI' },
            {title: '发稿属性',  dataIndex: 'sentenceAttribute', key: 'sentenceAttribute' },
            {title: '备注', dataIndex: 'remarks', key: 'remarks'},
            {
                title: '操作',
                key: 'action',
                fixed: 'right',
                width: 100,
                render: (text, record) => (
                    <div>
                        <Button type="danger" disabled>删除</Button>
                    </div>
                ),
            },
        ],
        data: [
            {
                key: '1',
                id: '1', //编号
                platformName: '于艾希Icey', //平台名
                platform:'微博', // 平台
                platformID:'weibo',
                area: '南区',
                city:'--',//
                kolTag: '--',//KOL tag
                pretestKII: 67,//前测KII
                fansCount:'1161271',//粉丝数
                pastCooperationCosts:'--',//过往合作费用
                averageReading:'12344',//平均阅读量
                lastReadingWithWatsons:'--',//与屈臣氏上次合作阅读量
                averageInteractionNumber:'--',//平均互动数
                lastInteractionWithWatsons:'--',//与屈臣氏上次合作互动数
                Links:'http://weibo.com/1815145485/Gtf3qgbEs', //账户ID或主页链接
                level:'1',//级别
                effectiveFanRate: '12.41%',//有效粉丝率
                effectiveFanCount: 144113,//有效粉丝数
                cooperationFee: '--',//合作费用（最新）
                estimatedTax: '--',//预估个税
                recentPostLinkWX: '--', //最近帖子链接（微信）
                estimatedReadings: '--',//预估阅读数
                estimatedInteractionNumber: '--',//预估互动数
                CPR: '--',
                CPC: '--', //每次点击付费广告
                ROI: '--', //投资回报率
                sentenceAttribute: '--',//发稿属性
                remarks: '--​',//备注
            },
            {
                key: '2',
                id: '2', //编号
                platformName: '小李丽莎V', //平台名
                platform:'微博', // 平台
                platformID:'weibo',
                area: '南区',
                city:'--',//
                kolTag: '--',//KOL tag
                pretestKII: 66,//前测KII
                fansCount:'572800',//粉丝数
                pastCooperationCosts:'--',//过往合作费用
                averageReading:'12344',//平均阅读量
                lastReadingWithWatsons:'--',//与屈臣氏上次合作阅读量
                averageInteractionNumber:'--',//平均互动数
                lastInteractionWithWatsons:'--',//与屈臣氏上次合作互动数
                Links:'http://weibo.com/1737746645/Guuf1DQ7n', //账户ID或主页链接
                level:'1',//级别
                effectiveFanRate: '18.41%',//有效粉丝率
                effectiveFanCount: 105452,//有效粉丝数
                cooperationFee: '--',//合作费用（最新）
                estimatedTax: '--',//预估个税
                recentPostLinkWX: '--', //最近帖子链接（微信）
                estimatedReadings: '--',//预估阅读数
                estimatedInteractionNumber: '--',//预估互动数
                CPR: '--',
                CPC: '--', //每次点击付费广告
                ROI: '--', //投资回报率
                sentenceAttribute: '--',//发稿属性
                remarks: '-- '
            }
        ],
    },
};
