import React from 'react';
import {
    Table, Divider, Icon, Input, InputNumber, Breadcrumb, Form, Row, Button, Col, Modal, Select } from 'antd';
import AddPlan from './Forms/AddPlan.js';
import Link from 'umi/link';
import { connect } from 'dva';

const FormItem = Form.Item;
const EditableContext = React.createContext();
const Option = Select.Option;
const namespace = 'plans';
const mapStateToProps = state => {
    const planData = state[namespace];
    return {
        planData
    };
};

const AddPlanForm = Form.create()(AddPlan);
function handleChange(value) {
   // console.log(`selected ${value}`);
}
const CollectionCreateForm = Form.create()(
    class extends React.Component {
        render() {
            const { visible, onCancel, onCreate, form } = this.props;
            return (
                <Modal
                    visible={visible}
                    title="创建新计划"
                    okText="创建"
                    cancelText="取消"
                    onCancel={onCancel}
                    onOk={onCreate}
                >
                    <AddPlanForm></AddPlanForm>
                </Modal>
            );
        }
    }
);

const EditableRow = ({ form, index, ...props }) => (
    <EditableContext.Provider value={form}>
      <tr {...props} />
    </EditableContext.Provider>
);
const EditableFormRow = Form.create()(EditableRow);
class EditableCell extends React.Component {
    getInput = () => {
        if (this.props.inputType === 'number') {
            return <InputNumber />;
        }
        return <Input />;
    };

    render() {
        const { editing, dataIndex, title, inputType, record, index, ...restProps } = this.props;
        return (
            <EditableContext.Consumer>
                {form => {
                    const { getFieldDecorator } = form;
                    return (
                        <td {...restProps}>
                            {editing ? (
                                <FormItem style={{ margin: 0 }}>
                                    {getFieldDecorator(dataIndex, {
                                        rules: [
                                            {
                                                required: true,
                                                message: `Please Input ${title}!`,
                                            },
                                        ],
                                        initialValue: record[dataIndex],
                                    })(this.getInput())}
                                </FormItem>
                            ) : (
                                restProps.children
                            )}
                        </td>
                    );
                }}
            </EditableContext.Consumer>
        );
    }
}

@connect(mapStateToProps)
export default class Plan extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            planTablecData: this.props.planData.planTablecData,
            editingKey: '',
            scrollWidth: '190%',
            newPlanModalVisible: false,
        };
        const operation = {
            title: '操作',
            dataIndex: 'operation',
            fixed: 'right',
            width: 200,
            render: (text, record) => {
                const editable = this.isEditing(record);
                return (
                    <div>
                        {editable ? (
                            <span>
                                      <EditableContext.Consumer>
                                        {form => (
                                            <a
                                                href="javascript:;"
                                                onClick={() => this.save(form, record.key)}
                                                style={{ marginRight: 8 }}
                                            >
                                                <Icon type="save" theme="outlined" />
                                                <span style={{ marginLeft: '6px' }}>保存</span>
                                            </a>
                                        )}
                                      </EditableContext.Consumer>
                                      <Divider type="vertical" />
                                      <a href="javascript:void(0);" onClick={() => this.cancel(record.key)}>
                                        <Icon type="close" theme="outlined" />
                                        <span style={{ marginLeft: '6px' }}>取消</span>
                                      </a>
                            </span>
                        ) : (
                            <span>
                              <a onClick={() => this.edit(record.key)}>
                                <Icon type="edit" theme="outlined" />
                                <span style={{ marginLeft: '6px' }}>编辑</span>
                              </a>
                              <Divider type="vertical" />
                              <a onClick={() => this.edit(record.key)}>
                                <Icon type="export" theme="outlined" />
                                <span style={{ marginLeft: '6px' }}>导出Excel</span>
                              </a>
                            </span>
                        )}
                    </div>
                );
            }
        };
        const tableColumns = this.props.planData.planTablecColumns;
        if(tableColumns.length=17){
            delete tableColumns[17];
            tableColumns.push(operation);
       }
        this.planTablecColumns =tableColumns;
    }
    // 新增 计划 弹窗相关方法
    saveFormRef = formRef => {
        this.formRef = formRef;
    };
    showModal = () => {
        this.setState({
            newPlanModalVisible: true,
        });
    };
    handleCreate = e => {
        this.setState({
            newPlanModalVisible: false,
        });
    };
    handleCancel = e => {
        this.setState({
            newPlanModalVisible: false,
        });
    };
    // 新增 计划 弹窗相关方法 END
    isEditing = record => {
        return record.key === this.state.editingKey;
    };
    edit(key) {
        this.setState({ editingKey: key, scrollWidth: '300%' });
    }
    save(form, key) {
        form.validateFields((error, row) => {
            if (error) {
                return;
            }
            const newData = [...this.state.planTablecData];
            const index = newData.findIndex(item => key === item.key);
            if (index > -1) {
                const item = newData[index];
                newData.splice(index, 1, {
                    ...item,
                    ...row,
                });
                this.setState({ planTablecData: newData, editingKey: '', scrollWidth: '190%' });
            } else {
                newData.push(row);
                this.setState({ planTablecData: newData, editingKey: '', scrollWidth: '190%' });
            }
        });
    }

    cancel = () => {
        this.setState({ editingKey: '', scrollWidth: '190%' });
    };
    render() {
        const components = {
            body: {
                row: EditableFormRow,
                cell: EditableCell,
            },
        };
        const planTablecColumns = this.planTablecColumns.map(col => {
            if (!col.editable) {
                return col;
            }
            return {
                ...col,
                onCell: record => ({
                    record,
                    inputType: 'text',
                    dataIndex: col.dataIndex,
                    title: col.title,
                    editing: this.isEditing(record),
                }),
            };
        });

        return (
            <div>
                <Breadcrumb>
                    <Breadcrumb.Item href="/">
                        <Icon type="home" />
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <Link to="/project">
                            <Icon type="pushpin" />
                            <span>项目</span>
                        </Link>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        1115selling kit
                    </Breadcrumb.Item>
                </Breadcrumb>
                <Divider />
              <Table
                  columns= {this.props.planData.projectTablecColumns}
                  dataSource={this.props.planData.projectTablecData}
                  pagination={false}
                  bordered
              />
              <Row type="flex" justify="space-between" style={{ marginTop: '15px' }}>
                <Col>
                  <h2 style={{ color: '#555', marginTop: '15px' }}>1115selling kit的计划</h2>
                </Col>
                <Col>
                  <Button type="primary" style={{ marginTop: '15px' }} onClick={this.showModal}>
                    添加计划
                  </Button>
                </Col>
              </Row>
              <Table
                  components={components}
                  bordered
                  dataSource={this.state.planTablecData}
                  columns={planTablecColumns}
                //  expandedRowRender={expandedRowRender}  //有子级内容的
                  rowClassName="editable-row"
                 // scroll={{ x: this.state.scrollWidth }}//滚动
              />
              <CollectionCreateForm
                  wrappedComponentRef={this.saveFormRef}
                  visible={this.state.newPlanModalVisible}
                  onCancel={this.handleCancel}
                  onCreate={this.handleCreate}
              />
            </div>
        );
    }
}
