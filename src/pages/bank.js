import React from 'react';
import { Table, Divider, Tag, Button } from 'antd';
import Link from 'umi/link';

const columns = [{
    title: '银行',
    dataIndex: 'name',
    key: 'name',
  //  render: text => <a href="javascript:;">{text}</a>,
    render: text =>
        <Link to={
            {
                pathname:"/bank/detail/anyParams",
                state:{data:{text}}
            }
        } >{text}</Link>,
        // <Link to="/bank/detail/anyParams">{text}</Link>,
}, {
    title: 'ENName',
    dataIndex: 'enname',
    key: 'enname',
}, {
    title: 'Address',
    dataIndex: 'address',
    key: 'address',
}, {
    title: 'Tags',
    key: 'tags',
    dataIndex: 'tags',
    render: tags => (
        <span>
      {tags.map(tag => <Tag color="blue" key={tag}>{tag}</Tag>)}
    </span>
    ),
}, {
    title: '操作',
    key: 'action',
    render: (text, record) => (
       <span>
         <a href="javascript:void(0);">Edit {record.name}</a>
         <Divider type="vertical" />
         <a href="javascript:void(0);">Delete</a>
       </span>
    )
}];

const data = [{
    key: '1',
    name: '农业银行',
    enname: 'ABCHINA',
    address: 'London No. 1 Lake P',
    tags: ['nice', 'developer'],
}, {
    key: '2',
    name: '中国银行',
    enname: 'BOC',
    address: 'London No. 1 Lake Park',
    tags: ['loser'],
}, {
    key: '3',
    name: '工商银行',
    enname: 'ICBC',
    address: 'Sidney No. 1 Lake Park',
    tags: ['cool', 'teacher'],
}];
export default class bank extends React.Component {
    handleAdd  = (e) => {
            console.log('add bank')
    }
    render() {
         return (
             <div>
                 <h2>所有银行</h2>
                 <Divider />
                 {<Table columns={columns} dataSource={data} />}
                 <Button block type="primary" onClick={this.handleAdd}>添加银行</Button>
             </div>

             )
    }
}
