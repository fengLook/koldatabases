import React from 'react';
import { Table, Button, Divider, Form, Switch, Row, Col, Input, Select } from 'antd';

const FormItem = Form.Item;
const InputGroup = Input.Group;
const Option = Select.Option;

const selectBefore = (
  <Select defaultValue="Http://" style={{ width: 90 }}>
    <Option value="Http://">Http://</Option>
    <Option value="Https://">Https://</Option>
  </Select>
);

const columns = [
  {
    title: '姓名',
    dataIndex: 'name',
    render: text => <a href="javascript:void(0);">{text}</a>,
  },
  {
    title: '平台',
    dataIndex: 'platform',
  },
  {
    title: '区域',
    dataIndex: 'region',
  },
  {
    title: '所在地',
    dataIndex: 'local',
  },
  {
    title: '黑名单',
    dataIndex: 'isBlacklist',
  },
  {
    title: '区域审核',
    dataIndex: 'regionReview',
  },
  {
    title: '总部审核',
    dataIndex: 'headquartersReview',
  },
  {
    title: '财务审核',
    dataIndex: 'financeReview',
  },
];
const data = [
  {
    key: '1',
    name: '小美',
    phone: '15622100666',
    platform: '小红书',
    region: '南区',
    local: '广东',
    isBlacklist: '否',
    regionReview: '已审核',
    headquartersReview: '已审核',
    financeReview: '已审核',
    address: '广州天河天河村',
  },
  {
    key: '2',
    name: '郭悠悠',
    phone: '15622100666',
    platform: '小红书',
    region: '南区',
    local: '广东',
    isBlacklist: '否',
    regionReview: '已审核',
    headquartersReview: '已审核',
    financeReview: '已审核',
    address: '广州天河天河村',
  },
  {
    key: '3',
    name: '韩冬冬',
    phone: '15622100666',
    platform: '小红书',
    region: '南区',
    local: '广东',
    isBlacklist: '否',
    regionReview: '已审核',
    headquartersReview: '已审核',
    financeReview: '已审核',
    address: '广州天河天河村',
  },
  {
    key: '4',
    name: '崔小绺',
    phone: '15622100666',
    platform: '小红书',
    region: '南区',
    local: '广东',
    isBlacklist: '否',
    regionReview: '已审核',
    headquartersReview: '已审核',
    financeReview: '已审核',
    address: '广州天河天河村',
  },
];

// rowSelection object indicates the need for row selection
const rowSelection = {
  onChange: (selectedRowKeys, selectedRows) => {
    console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
  },
  getCheckboxProps: record => ({
    name: record.name,
  }),
};

export default () => {
  return (
    <div>
      <Row gutter={16 + 6} style={{ marginBottom: '20px' }}>
        <Form layout="inline">
          <Col span={6}>
            <div>
              <Input addonBefore="姓名" defaultValue="小美" />
            </div>
          </Col>
          <Col span={6}>
            <div>
              <Input addonBefore="电话" defaultValue="15622100000" />
            </div>
          </Col>
          <Col span={4} style={{ textAlign: 'left' }}>
            <Button type="primary">搜索</Button>
          </Col>
        </Form>
      </Row>
      <Table columns={columns} dataSource={data} />
    </div>
  );
};
