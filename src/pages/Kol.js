import React from 'react';
import { Button, Divider, Form, Switch, Row, Col, Input, Select } from 'antd';
import { connect } from 'dva';
import KolList from './Forms/KolList';
import Link from 'umi/link';

const FormItem = Form.Item;
const InputGroup = Input.Group;
const Option = Select.Option;

const namespace = 'kols';

const selectBefore = (
  <Select defaultValue="Http://" style={{ width: 90 }}>
    <Option value="Http://">Http://</Option>
    <Option value="Https://">Https://</Option>
  </Select>
);

const mapStateToProps = state => {
  //console.log('state', state.kols);
  const kols = state.kols;
  return kols;
};

// rowSelection object indicates the need for row selection
const rowSelection = {
  onChange: (selectedRowKeys, selectedRows) => {
    console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
  },
  getCheckboxProps: record => ({
    name: record.name,
  }),
};
function handleChange(value) {
    console.log(`selected ${value}`);
}
const expandedRowRender = record => <p>{record.description}</p>;
const title = () => 'Here is title';
const showHeader = true;
const footer = () => 'Here is footer';
const scroll = { y: 240 };
const pagination = { position: 'bottom' };

@connect(mapStateToProps)
export default class Kol extends React.Component {
  handleToggle = prop => {
      console.log(`switch`);
    // return enable => {
    //   this.setState({ [prop]: enable });
    // };
  };

  render() {
    return (
      <div>
          <Form layout="inline">
            <Row gutter={24} style={{ marginBottom: '10px' }}>
            <Col span={6}>
              <div>
                <Input addonBefore="姓名" />
              </div>
            </Col>
            <Col span={6}>
              <div>
                <Input addonBefore="手机号码 " />
              </div>
            </Col>
            <Col span={6}>
              <div>
                <Input addonBefore="邮箱 " />
              </div>
            </Col>
              <Col span={6} gutter={24}>
                  {/*<Input addonBefore="区域 " />*/}
                  <span style={{textAlign:'left'}}>区域：</span>
                  <Select addonBefore="区域 " defaultValue="ho" style={{ width: 120 }} onChange={handleChange}>
                    <Option value="ho">ho</Option>
                    <Option value="north">north</Option>
                    {/*<Option value="disabled" disabled>Disabled</Option>*/}
                    <Option value="south">south</Option>
                    <Option value="west">west</Option>
                    <Option value="east">east</Option>
                  </Select>
              </Col>
            </Row>
            <Row gutter={24} style={{ marginBottom: '16px' }}>
              <Col span={6}>
                <div>
                  <Input addonBefore="平台 " />
                </div>
              </Col>
              <Col span={6}>
                <div>
                  <Input addonBefore="平台账户ID " />
                </div>
              </Col>
              <Col span={6}>
                <div>
                  <Input addonBefore="平台账户名 " />
                </div>
              </Col>
            <Col span={4}>
              <span>是否确认条款：</span>
              <Select style={{ width: 70 }} onChange={handleChange}>
                <Option value="YES">YES</Option>
                <Option value="NO">NO</Option>
              </Select>
            </Col>
            <Col span={2} style={{ textAlign: 'left', paddingLeft: '0' }}>
              <Button type="primary">搜索</Button>
            </Col>
            </Row>
          </Form>
        <Divider></Divider>
        <Row style={{ marginBottom: '10px' }}>
          <Button type="primary">批量审核</Button>
        </Row>
        <KolList isDisabled={true} rowSelection={rowSelection} columns={this.props.columns} dataSource={this.props.kolList}></KolList>
        {/*<Table*/}
            {/*rowSelection={rowSelection}*/}
            {/*columns={this.props.columns}*/}
            {/*dataSource={this.props.data}*/}
        {/*/>*/}
      </div>
    );
  }
}
