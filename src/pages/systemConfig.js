import React from 'react';
import { Table, Button, Divider, Form, Switch, Row, Col, Input, Select } from 'antd';

const FormItem = Form.Item;
const InputGroup = Input.Group;
const Option = Select.Option;

const selectBefore = (
  <Select defaultValue="Http://" style={{ width: 90 }}>
    <Option value="Http://">Http://</Option>
    <Option value="Https://">Https://</Option>
  </Select>
);

const columns = [
  {
    title: '名字',
    dataIndex: 'name',
    render: text => <a href="javascript:void(0);">{text}</a>,
  },
  {
    title: '英文名',
    dataIndex: 'enName',
  },
  {
    title: '输入类型',
    dataIndex: 'inputType',
  },
  {
    title: '输入范例',
    dataIndex: 'placehold',
  },
  {
    title: '是否必填',
    dataIndex: 'isRequired',
  },
  {
    title: '所属页面',
    dataIndex: 'page',
  },
  {
    title: '排序',
    dataIndex: 'index',
  },
  {
    title: '分组',
    dataIndex: 'group',
  },
  {
    title: '操作',
    key: 'action',
    render: (text, record) => (
      <div>
        <Button type="primary">编辑</Button>
        <Divider type="vertical" />
        <Button type="danger">删除</Button>
      </div>
    ),
  },
];
const data = [
  {
    key: '1',
    name: '开户行',
    enName: 'engagement',
    inputType: 'text/number',
    placehold: '例如：9666',
    isRequired: '是',
    page: 'platform',
    index: '7',
    group: '银行卡信息',
  },
  {
    key: '2',
    name: '身份证姓名',
    enName: 'nameByIDCard',
    inputType: 'text',
    placehold: '例如：李小晓',
    isRequired: '是',
    page: 'card',
    index: '2',
    group: '银行卡信息',
  },
  {
    key: '3',
    name: '银行卡持有人',
    enName: 'nameByBankCard',
    inputType: 'text',
    placehold: '例如：李小晓',
    isRequired: '是',
    page: 'card',
    index: '1',
    group: '银行卡信息',
  },
  {
    key: '4',
    name: '账户名',
    enName: 'accountName',
    inputType: 'text',
    placehold: '例如：小仙女',
    isRequired: '是',
    page: 'card',
    index: '4',
    group: '平台信息',
  },
];

// rowSelection object indicates the need for row selection
const rowSelection = {
  onChange: (selectedRowKeys, selectedRows) => {
    console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
  },
  getCheckboxProps: record => ({
    name: record.name,
  }),
};

export default () => {
  return (
    <div>
      <Row gutter={16 + 6} style={{ marginBottom: '16px' }}>
        <Form layout="inline">
          <Col span={6}>
            <div>
              <Input addonBefore="名字" defaultValue="开户行" />
            </div>
          </Col>
          <Col span={6}>
            <div>
              <Input addonBefore="所属页面" defaultValue="card" />
            </div>
          </Col>
          <Col span={4} style={{ textAlign: 'left' }}>
            <Button type="primary">搜索</Button>
          </Col>
        </Form>
      </Row>
      <Table columns={columns} dataSource={data} />
    </div>
  );
};
