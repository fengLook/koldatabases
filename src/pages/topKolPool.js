import React from 'react';
import { Tabs, Table, Row, Col, Rate, Icon, Input, Button, Form, Divider,Select, Modal } from 'antd';

import Link from 'umi/link';
import { connect } from 'dva';

const mapStateToProps = state => {
    const topKolPool = state['topKolPool'];
    const dataDetail = state['kolDetails'];
    return {
        topKolPool, dataDetail
    };


};
const TabPane = Tabs.TabPane;
const FormItem = Form.Item;
const Option = Select.Option;
function callback(key) {
    console.log(key);
}
function handleChange(value) {
   console.log(`selected ${value}`);
}

@connect(mapStateToProps)
export default class TopKolPool extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            qianCeModalVisible: false,
            houCeModalVisible: false,
        }
    }
// 前测 后测
    showHoCeModal = () => {
        this.setState({
            houCeModalVisible: true,
        });
    };
    showQianCeModal = () => {
        this.setState({
            qianCeModalVisible: true,
        });
    };
    houCeHandleOk = e => {
        this.setState({
            houCeModalVisible: false,
        });
    };
    qianCeHandleOk = e => {
        this.setState({
            qianCeModalVisible: false,
        });
    };
    houCehHandleCancel = e => {
        this.setState({
            houCeModalVisible: false,
        });
    };
    qianCehHandleCancel = e => {
        this.setState({
            qianCeModalVisible: false,
        });
    };
// 前测 后测 END
    render() {
        const operatingColumns = {
            title: '前测数据（最新）',
            dataIndex: 'pretestData',
            render:  (text) => {
                if(text>=30){
                    return <span><a href="javascript:void(0);" onClick={this.showQianCeModal}>{text}</a><Icon type="caret-up" style={{color:'#32CD32'}}/></span>
                }else  if(text>=0){
                    return <span><a href="javascript:void(0);" onClick={this.showQianCeModal}>{text}</a><Icon type="caret-down" style={{color:'#f00'}} /></span>
                }else {
                    return <span><a href="javascript:void(0);" onClick={this.showQianCeModal}>{text}</a></span>
                }
            },
        }
        const operatingAfterColumns = {
            title: '后测数据（最新）',
            dataIndex: 'postTestData',
            render: (text) => {
                if(text>=30){
                    return <span><a href="javascript:void(0);" onClick={this.showHoCeModal}>{text}</a>
                        <Icon type="caret-up" style={{color:'#32CD32'}}/>
                        </span>
                }else {
                    return <span><a href="javascript:void(0);" onClick={this.showHoCeModal}>{text}</a>
                        <Icon type="caret-down" style={{color:'#f00'}} />
                        </span>
                }
            },
        }
        let kolColumnsDH = this.props.topKolPool.kolColumnsDH;
        let kolColumnsZC = this.props.topKolPool.kolColumnsZC;
        let kolColumnsPPBG = this.props.topKolPool.kolColumnsPPBG;
        kolColumnsPPBG[3]= operatingColumns;
        kolColumnsPPBG[4]= operatingAfterColumns;
        kolColumnsZC[3]= operatingColumns;
        kolColumnsZC[4]= operatingAfterColumns;
        kolColumnsDH[3] = operatingColumns;
        kolColumnsDH[4]= operatingAfterColumns;
        const kolDetailsDH = this.props.topKolPool.dataDH;
        const kolDetailsZC = this.props.topKolPool.dataZC;
        const kolDetailsPPBG = this.props.topKolPool.dataPPBG;

        const kolTestColumns = this.props.dataDetail.kolTestColumns;
        const kolTestDetails = this.props.dataDetail.kolDetail;
        return <div>
                    <Tabs defaultActiveKey="1" onChange={callback}>
                        <TabPane tab="品牌曝光" key="1">
                            <Row gutter={24} style={{ marginBottom: '16px' }}>
                                <Form layout="inline">
                            <Col span={6}>
                                <div>
                                    <Input addonBefore="Platform平台" defaultValue="" />
                                </div>
                            </Col>
                            <Col span={6}>
                                <div>
                                    <Input addonBefore="合作费用" defaultValue="" />
                                </div>
                            </Col>
                            <Col span={4}>
                                <Input addonBefore="平均ROI" defaultValue="" />
                            </Col>
                            <Col span={4}>
                                {/*<Input addonBefore="星级" defaultValue="" />*/}
                                    <span style={{textAlign:'left'}}>星级：</span>
                                    <Select defaultValue="一星" style={{ width: 120 }} onChange={this.handleChange}>
                                        <Option value="一星">一星</Option>
                                        {/*<Option value="二星">二星</Option>*/}
                                        <Option value="三星">三星</Option>
                                        {/*<Option value="四星">四星</Option>*/}
                                        <Option value="五星">五星</Option>
                                    </Select>
                            </Col>
                            <Col span={4} style={{ textAlign: 'left', paddingLeft: '0' }}>
                                <Button type="primary">搜索</Button>
                            </Col>
                        </Form>
                        </Row>
                            <Divider></Divider>
                            <Table columns={kolColumnsPPBG} dataSource={kolDetailsPPBG} showHeader={true} pagination={false} size='small' />
                        </TabPane>
                        <TabPane tab="种草" key="2">
                            <Row gutter={24} style={{ marginBottom: '16px' }}>
                                <Form layout="inline">
                                    <Col span={6}>
                                        <div>
                                            <Input addonBefore="Platform平台" defaultValue="" />
                                        </div>
                                    </Col>
                                    <Col span={6}>
                                        <div>
                                            <Input addonBefore="合作费用" defaultValue="" />
                                        </div>
                                    </Col>
                                    <Col span={4}>
                                        <Input addonBefore="平均ROI" defaultValue="" />
                                    </Col>
                                    <Col span={4}>
                                        {/*<Input addonBefore="星级" defaultValue="" />*/}
                                        <span style={{textAlign:'left'}}>星级：</span>
                                        <Select defaultValue="一星" style={{ width: 120 }} onChange={this.handleChange}>
                                            <Option value="一星">一星</Option>
                                            {/*<Option value="二星">二星</Option>*/}
                                            <Option value="三星">三星</Option>
                                            {/*<Option value="四星">四星</Option>*/}
                                            <Option value="五星">五星</Option>
                                        </Select>
                                    </Col>
                                    <Col span={4} style={{ textAlign: 'left', paddingLeft: '0' }}>
                                        <Button type="primary">搜索</Button>
                                    </Col>
                                </Form>
                            </Row>
                            <Table columns={kolColumnsZC} dataSource={kolDetailsZC} showHeader={true} pagination={false} size='small' />
                        </TabPane>
                        <TabPane tab="带货" key="3">
                            <Row gutter={24} style={{ marginBottom: '16px' }}>
                                <Form layout="inline">
                                    <Col span={6}>
                                        <div>
                                            <Input addonBefore="Platform平台" defaultValue="" />
                                        </div>
                                    </Col>
                                    <Col span={6}>
                                        <div>
                                            <Input addonBefore="合作费用" defaultValue="" />
                                        </div>
                                    </Col>
                                    <Col span={4}>
                                        <Input addonBefore="平均ROI" defaultValue="" />
                                    </Col>
                                    <Col span={4}>
                                        {/*<Input addonBefore="星级" defaultValue="" />*/}
                                        <span style={{textAlign:'left'}}>星级：</span>
                                        <Select defaultValue="一星" style={{ width: 120 }} onChange={this.handleChange}>
                                            <Option value="一星">一星</Option>
                                            {/*<Option value="二星">二星</Option>*/}
                                            <Option value="三星">三星</Option>
                                            {/*<Option value="四星">四星</Option>*/}
                                            <Option value="五星">五星</Option>
                                        </Select>
                                    </Col>
                                    <Col span={4} style={{ textAlign: 'left', paddingLeft: '0' }}>
                                        <Button type="primary">搜索</Button>
                                    </Col>
                                </Form>
                            </Row>
                            <Table columns={kolColumnsDH} dataSource={kolDetailsDH} showHeader={true} pagination={true} size='small' />
                        </TabPane>
                    </Tabs>
                    <Modal
                        title={kolTestDetails.user+"微博项目前测数据"}
                        visible={this.state.qianCeModalVisible}
                        onOk={this.qianCeHandleOk}
                        onCancel={this.qianCehHandleCancel}
                        footer={null}
                    >
                        <Table columns={kolTestColumns} dataSource={kolTestDetails.testData} showHeader={false} pagination={false} size='small' />
                    </Modal>
                    <Modal
                        title={kolTestDetails.user+"微博项目后测数据"}
                        visible={this.state.houCeModalVisible}
                        onOk={this.houCeHandleOk}
                        onCancel={this.houCehHandleCancel}
                        footer={null}
                    >
                        <Table columns={kolTestColumns} dataSource={kolTestDetails.testDataAfter} showHeader={false} pagination={false} size='small'/>
                    </Modal>
                </div>
    }
}
