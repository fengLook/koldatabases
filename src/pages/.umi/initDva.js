import dva from 'dva';
import createLoading from 'dva-loading';

const runtimeDva = window.g_plugins.mergeConfig('dva');
let app = dva({
  history: window.g_history,
  
  ...(runtimeDva.config || {}),
});

window.g_app = app;
app.use(createLoading());
(runtimeDva.plugins || []).forEach(plugin => {
  app.use(plugin);
});

app.model({ namespace: 'kol', ...(require('D:/Watsons/仓库/gitKOL/kol_databases/src/model/kol.js').default) });
app.model({ namespace: 'kolDetail', ...(require('D:/Watsons/仓库/gitKOL/kol_databases/src/model/kolDetail.js').default) });
app.model({ namespace: 'leaderboard', ...(require('D:/Watsons/仓库/gitKOL/kol_databases/src/model/leaderboard.js').default) });
app.model({ namespace: 'login', ...(require('D:/Watsons/仓库/gitKOL/kol_databases/src/model/login.js').default) });
app.model({ namespace: 'plan', ...(require('D:/Watsons/仓库/gitKOL/kol_databases/src/model/plan.js').default) });
app.model({ namespace: 'planDetail', ...(require('D:/Watsons/仓库/gitKOL/kol_databases/src/model/planDetail.js').default) });
app.model({ namespace: 'project', ...(require('D:/Watsons/仓库/gitKOL/kol_databases/src/model/project.js').default) });
app.model({ namespace: 'topKolPool', ...(require('D:/Watsons/仓库/gitKOL/kol_databases/src/model/topKolPool.js').default) });
