import React from 'react';
import { Router as DefaultRouter, Route, Switch } from 'react-router-dom';
import dynamic from 'umi/dynamic';
import renderRoutes from 'umi/_renderRoutes';


let Router = require('dva/router').routerRedux.ConnectedRouter;

let routes = [
  {
    "path": "/user",
    "redirect": "/user/login",
    "exact": true
  },
  {
    "path": "/",
    "redirect": "/dashboard",
    "exact": true
  },
  {
    "path": "/user",
    "component": require('../../layout/UserLayout').default,
    "routes": [
      {
        "path": "/user/login",
        "component": require('../User/Login').default,
        "exact": true
      },
      {
        "component": () => React.createElement(require('D:/Watsons/仓库/gitKOL/kol_databases/node_modules/umi-build-dev/lib/plugins/404/NotFound.js').default, { pagesPath: 'src/pages', hasRoutesInConfig: true })
      }
    ]
  },
  {
    "path": "/",
    "component": require('../../layout').default,
    "Routes": [require('../Authorized').default],
    "authority": [
      "admin",
      "user"
    ],
    "routes": [
      {
        "path": "/dashboard",
        "name": "dashboard",
        "icon": "dashboard",
        "routes": [
          {
            "path": "/dashboard",
            "name": "dashboard",
            "component": require('../Dashboard').default,
            "exact": true
          },
          {
            "path": "/dashboard/kol",
            "routes": [
              {
                "path": "/kol",
                "authority": [
                  "admin"
                ],
                "component": require('../Kol').default,
                "exact": true
              },
              {
                "path": "/kol/detail/:page",
                "hideInMenu": true,
                "name": "detail",
                "component": require('../KolDetail').default,
                "exact": true
              },
              {
                "component": () => React.createElement(require('D:/Watsons/仓库/gitKOL/kol_databases/node_modules/umi-build-dev/lib/plugins/404/NotFound.js').default, { pagesPath: 'src/pages', hasRoutesInConfig: true })
              }
            ]
          },
          {
            "path": "/dashboard/old_kol",
            "component": require('../OldKol').default,
            "exact": true
          },
          {
            "path": "/dashboard/Platform",
            "component": require('../Platform').default,
            "exact": true
          },
          {
            "path": "/dashboard/system_config",
            "component": require('../SystemConfig').default,
            "exact": true
          },
          {
            "path": "/dashboard/region_code",
            "component": require('../RegionCode').default,
            "exact": true
          },
          {
            "path": "/dashboard/project",
            "routes": [
              {
                "path": "/project",
                "component": require('../Project').default,
                "exact": true
              },
              {
                "path": "/project/plan/:page",
                "hideInMenu": true,
                "name": "plan",
                "component": require('../Plan').default,
                "exact": true
              },
              {
                "path": "/project/planDetail/:page",
                "hideInMenu": true,
                "name": "planDetail",
                "component": require('../PlanDetail').default,
                "exact": true
              },
              {
                "component": () => React.createElement(require('D:/Watsons/仓库/gitKOL/kol_databases/node_modules/umi-build-dev/lib/plugins/404/NotFound.js').default, { pagesPath: 'src/pages', hasRoutesInConfig: true })
              }
            ]
          },
          {
            "path": "/dashboard/bank",
            "routes": [
              {
                "path": "/bank",
                "component": require('../Bank').default,
                "exact": true
              },
              {
                "path": "/bank/detail/:page",
                "hideInMenu": true,
                "name": "detail",
                "component": require('../BankDetail').default,
                "exact": true
              },
              {
                "component": () => React.createElement(require('D:/Watsons/仓库/gitKOL/kol_databases/node_modules/umi-build-dev/lib/plugins/404/NotFound.js').default, { pagesPath: 'src/pages', hasRoutesInConfig: true })
              }
            ]
          },
          {
            "path": "/dashboard/tags",
            "component": require('../Tags').default,
            "exact": true
          },
          {
            "path": "/dashboard/topKolPool",
            "component": require('../TopKolPool').default,
            "exact": true
          },
          {
            "component": () => React.createElement(require('D:/Watsons/仓库/gitKOL/kol_databases/node_modules/umi-build-dev/lib/plugins/404/NotFound.js').default, { pagesPath: 'src/pages', hasRoutesInConfig: true })
          }
        ]
      },
      {
        "component": () => React.createElement(require('D:/Watsons/仓库/gitKOL/kol_databases/node_modules/umi-build-dev/lib/plugins/404/NotFound.js').default, { pagesPath: 'src/pages', hasRoutesInConfig: true })
      }
    ]
  },
  {
    "component": () => React.createElement(require('D:/Watsons/仓库/gitKOL/kol_databases/node_modules/umi-build-dev/lib/plugins/404/NotFound.js').default, { pagesPath: 'src/pages', hasRoutesInConfig: true })
  }
];
window.g_plugins.applyForEach('patchRoutes', { initialValue: routes });

export default function() {
  return (
<Router history={window.g_history}>
      { renderRoutes(routes, {}) }
    </Router>
  );
}
