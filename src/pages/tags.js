import React from 'react';
import { Table, Divider, Icon } from 'antd';
import Link from 'umi/link';

const columns = [{
    title: '标签名',
    dataIndex: 'name',
    key: 'name',
}, {
    title: 'ENName',
    dataIndex: 'enname',
    key: 'enname',
},  {
    title: '操作',
    key: 'action',
    render: (text, record) => (
       <span>
         <a href="javascript:void(0);">Edit {record.name}</a>
         <Divider type="vertical" />
         <a href="javascript:void(0);">Delete</a>
       </span>
    )
}];
const data = [{
    key: '1',
    name: 'cool',
    enname: 'cool',
}, {
    key: '2',
    name: '美食',
    enname: 'Food',
}, {
    key: '3',
    name: '时尚',
    enname: 'Beauty',
}];
export default class tags extends React.Component {
    render() {
         return (
             <div>
                 <h2>所有标签</h2>
                 <Divider />
                 <div>
                     <Link to="/">
                         <Icon type="plus" />
                         <span>&nbsp;新建标签</span>
                     </Link>
                 </div>
                 {<Table columns={columns} dataSource={data} />}
             </div>
             )
    }
}
