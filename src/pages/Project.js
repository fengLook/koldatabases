import React from 'react';
import {
  Table,
  Modal,
  Button,
  message,
  Divider,
  Form,
  Switch,
  Row,
  Col,
  Input,
  Select,
  Dropdown,
  Menu,
  Icon,
} from 'antd';
import { connect } from 'dva';
import Link from 'umi/link';
import AddProject from './Forms/AddProject.js';
const FormItem = Form.Item;
const InputGroup = Input.Group;
const Option = Select.Option;
const namespace = 'project';

const menu = (
  <Menu>
    <Menu.Item>
      <Icon type="eye" theme="outlined" />
      查看报告
    </Menu.Item>
    <Menu.Item>
      <Icon type="upload" theme="outlined" />
      上传报告
    </Menu.Item>
  </Menu>
);
const confirmDone = Modal.confirm;//是否审核 对话框
const confirmBatchDone = Modal.confirm;// 批量审核 对话框
let onSelectedRows = [];
const rowSelection = {
          onChange: (selectedRowKeys, selectedRows) => {
            // console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
              onSelectedRows = selectedRows;
          },
          getCheckboxProps: record => ({
            name: record.name,
          }),
};
const mapStateToProps = state => {
    const dataProject = state[namespace];
    return {
        dataProject
    };
};

const AddProjectForm = Form.create()(AddProject);
const CollectionCreateForm = Form.create()(
    class extends React.Component {
        render() {
            const { visible, onCancel, onCreate, form } = this.props;
            const { getFieldDecorator } = form;
            return (
                <Modal
                    visible={visible}
                    title="创建新项目"
                    okText="创建"
                    cancelText="取消"
                    onCancel={onCancel}
                    onOk={onCreate}
                >
                    <AddProjectForm></AddProjectForm>
                </Modal>
            );
        }
    }
);
@connect(mapStateToProps)
export default class Kol extends React.Component {
    state = {
      loading: false,
      isAgree: true,
      selectedRows: [],
      newPlanModalVisible: false,
      columns: [
        {
          title: '项目编号',
          dataIndex: 'code',
          fixed: 'left',
          width: 120,
          render: code => <Link to="/project/plan/anyParams">{code}</Link>,
        },
        {
          title: '项目名称',
          dataIndex: 'name',
        },
        {
          title: '开始时间',
          dataIndex: 'startTime',
        },
        {
          title: '参与区域',
          dataIndex: 'region',
        },
        {
          title: '项目状态',
          dataIndex: 'status',
        },
        {
          title: 'Champion审核情况',
          dataIndex: 'championReview',
        },
        {
          title: 'HO审核情况',
          dataIndex: 'hoReview',
        },
        {
          title: '不通过原因',
          dataIndex: 'reason',
        },
          {
              title: '操作',
              key: 'action',
              fixed: 'right',
              width: 240,
              render: (text, record) => (
                  <div>
                    <Link to="/project/plan/anyParams">
                      <Icon type="calendar" theme="outlined" />
                      <span style={{ marginLeft: '6px' }}>计划</span>
                    </Link>
                    <Divider type="vertical" />
                    <a onClick={() => this.handleDone(record)}>
                      <Icon type="check" theme="outlined" />
                      <span style={{ marginLeft: '6px' }}>审核</span>
                    </a>
                    <Divider type="vertical" />
                    <Dropdown overlay={menu}>
                      <a href="javascript:void(0);">
                        <span style={{ marginLeft: '6px' }}>报告</span> <Icon type="down" />
                      </a>
                    </Dropdown>
                      {/* <a href="javascript:;">报告</a> */}

                  </div>
              ),
        },
    ],
  };
    handleDone = (record) => {
        confirmDone({
            title: '确定审核项目' + record.name +'?',
            content: '项目开始时间：'+record.startTime,
            onOk() {
                console.log('OK');
            },
            onCancel() {
                console.log('Cancel');
            },
        });
    };

  handleToggle = prop => {
    return enable => {
      this.setState({ [prop]: enable });
    };
  };
    handleBatchDone = () => {
      //  console.log(`selectedRowKeys: `+onSelectedRows);
        let itemName = '';
        if(onSelectedRows.length>0){
                        for(var i=0;i<onSelectedRows.length;i++){
                            if(i+1==onSelectedRows.length){
                                    itemName = itemName+'【'+onSelectedRows[i].name + '】';
                            }else {
                                    itemName = itemName+'【'+onSelectedRows[i].name + '】、';
                            }
                        }
                        confirmDone({
                            title: '确定审核项目?',
                            content: '项目：'+  itemName,
                            onOk() {
                                console.log('OK');
                            },
                            onCancel() {
                                console.log('Cancel');
                            },
                        });
        }else {
            message.info('当前无选择项目，请确认已选择！');
        }
    };
    handleAddProject=()=>{
        this.setState({
            newPlanModalVisible: true,
        });
    }
    saveFormRef = formRef => {
        this.formRef = formRef;
    };
    handleOk = e => {
        this.setState({
            newPlanModalVisible: false,
        });
    };
    handleCancel = e => {
        this.setState({
            newPlanModalVisible: false,
        });
    };
  render() {
    return (
      <div>
        <Row gutter={16 + 6} style={{ marginBottom: '16px' }}>
          <Form layout="inline">
            <Col span={6}>
              <div>
                <Input addonBefore="项目编号" defaultValue="" />
              </div>
            </Col>
            <Col span={6}>
              <div>
                <Input addonBefore="项目名称" defaultValue="" />
              </div>
            </Col>
            <Col span={4}>
              <FormItem label="是否审核" style={{ marginTop: '-4px' }}>
                <Switch checked={this.state.isAgree} onChange={this.handleToggle('isAgree')} />
              </FormItem>
            </Col>
            <Col span={4} style={{ textAlign: 'left', paddingLeft: '0' }}>
              <Button type="primary">搜索</Button>
            </Col>
          </Form>
        </Row>
         <Divider></Divider>
        <Row style={{ marginBottom: '10px' }}>
            <Button onClick={this.handleBatchDone} type="primary">批量审核</Button>
            <Button style={{margin: '0 0 0 10px'}} onClick={this.handleAddProject}>新增项目</Button>
        </Row>
        <Table
          rowSelection={rowSelection}
          scroll={{ x: '115%' }}
          columns={this.state.columns}
          dataSource={this.props.dataProject.data}
        />
          <CollectionCreateForm
              wrappedComponentRef={this.saveFormRef}
              visible={this.state.newPlanModalVisible}
              onCancel={this.handleCancel}
              onCreate={this.handleCreate}
          />
      </div>
    );
  }
}
