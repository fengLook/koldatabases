import { Card, Col, Row, Icon } from 'antd';
const { Meta } = Card;

export default () => {
  return (
    <div>
      <Row gutter={24}>
        <Col span={6}>
          <Card
            style={{ width: 240, marginBottom: '20px' }}
            actions={[<Icon type="edit" />, <Icon type="delete" />]}
          >
            <Meta title="快牛直播" description="kuainiuzhibo" />
          </Card>
        </Col>
        <Col span={6}>
          <Card
            style={{ width: 240, marginBottom: '20px' }}
            actions={[<Icon type="edit" />, <Icon type="delete" />]}
          >
            <Meta title="小红书" description="xiaohongshu" />
          </Card>
        </Col>
        <Col span={6}>
          <Card
            style={{ width: 240, marginBottom: '20px' }}
            actions={[<Icon type="edit" />, <Icon type="delete" />]}
          >
            <Meta title="快手直播" description="kuaishou" />
          </Card>
        </Col>
        <Col span={6}>
          <Card
            style={{ width: 240, marginBottom: '20px' }}
            actions={[<Icon type="edit" />, <Icon type="delete" />]}
          >
            <Meta title="微任务" description="weirenwu" />
          </Card>
        </Col>
        <Col span={6}>
          <Card
            style={{ width: 240, marginBottom: '20px' }}
            actions={[<Icon type="edit" />, <Icon type="delete" />]}
          >
            <Meta title="微淘" description="weitao" />
          </Card>
        </Col>
        <Col span={6}>
          <Card
            style={{ width: 240, marginBottom: '20px' }}
            actions={[<Icon type="edit" />, <Icon type="delete" />]}
          >
            <Meta title="美拍" description="meipai" />
          </Card>
        </Col>
        <Col span={6}>
          <Card
            style={{ width: 240, marginBottom: '20px' }}
            actions={[<Icon type="edit" />, <Icon type="delete" />]}
          >
            <Meta title="抖音" description="douyin" />
          </Card>
        </Col>
        <Col span={6}>
          <Card
            style={{ width: 240, marginBottom: '20px' }}
            actions={[<Icon type="edit" />, <Icon type="delete" />]}
          >
            <Meta title="映客" description="yingke" />
          </Card>
        </Col>
        <Col span={6}>
          <Card
            style={{ width: 240, marginBottom: '20px' }}
            actions={[<Icon type="edit" />, <Icon type="delete" />]}
          >
            <Meta title="花椒" description="huajiao" />
          </Card>
        </Col>
        <Col span={6}>
          <Card
            style={{ width: 240, marginBottom: '20px' }}
            actions={[<Icon type="edit" />, <Icon type="delete" />]}
          >
            <Meta title="其他" description="orther" />
          </Card>
        </Col>
      </Row>
    </div>
  );
};
