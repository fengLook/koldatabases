import React, { Component } from 'react';
import { Table, Breadcrumb, Divider, Icon, Form,  Row, Input, Select, Tabs, Rate, Modal } from 'antd';
import { connect } from 'dva';
import PlanDetailForm from './Forms/PlanDetail.js';
import Link from 'umi/link';

const mapStateToProps = state => {
    const kols = state.kols;
    return kols;
};

const PlanDetailFormTable = Form.create()(PlanDetailForm)
@connect(mapStateToProps)
export default class PlanDetail extends Component {
    render() {
        return (
            <div>
                <Breadcrumb>
                    <Breadcrumb.Item href="/">
                        <Icon type="home" />
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <Link to="/project">
                            <Icon type="pushpin" />
                            <span>项目</span>
                        </Link>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <Link to="/project/plan/anyParams">
                            <Icon type="file-text" />
                            <span>计划</span>
                        </Link>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        1115 selling kit SC{/*{kolDetails.user}*/}
                    </Breadcrumb.Item>
                </Breadcrumb>
                <Divider></Divider>
                <PlanDetailFormTable columns={this.props.popAddKolColumns} dataSource={this.props.kolList}></PlanDetailFormTable>
            </div>
        );
    }
}