import React, { Component } from 'react';
import { connect } from 'dva';
//import { formatMessage, FormattedMessage } from 'umi/locale';
import Link from 'umi/link';
import { Checkbox, Alert, Icon } from 'antd';
import Login from '@/components/Login';
import styles from './Login.less';
import fetch from 'dva/fetch';

const { Tab, UserName, Password, Mobile, Captcha, Submit } = Login;

@connect(({ login, loading }) => ({
    login,
    submitting: loading.effects['login/login'],
}))
class LoginPage extends Component {
    state = {
        type: 'account',
        autoLogin: true,
    };

    onTabChange = type => {
        this.setState({ type });
    };

    onGetCaptcha = () =>
        new Promise((resolve, reject) => {
            this.loginForm.validateFields(['mobile'], {}, (err, values) => {
                if (err) {
                    reject(err);
                } else {
                    const { dispatch } = this.props;
                    dispatch({
                        type: 'login/getCaptcha',
                        payload: values.mobile,
                    })
                        .then(resolve)
                        .catch(reject);
                }
            });
        });

    handleSubmit = (err, values) => {
        const { type } = this.state;
        if (!err) {
           const { dispatch } = this.props;
            dispatch({
                type: 'login/login',
                payload: {
                    ...values,
                    type,
                },
            });
           //  fetch('http://10.82.33.118:3001/parse/functions/login', {
           //      method: "POST",
           //      mode: "no-cors",
           //      headers: {
           //          "Content-Type": "application/x-www-form-urlencoded",
           //          "Origin":"http://10.82.33.118:3008",
           //          "Referer":"http://10.82.33.118:3008/index.html",
           //          "X-Parse-Application-Id":"d973efad-e3a5-4793-99f6-807c68e89646",
           //          "-Parse-REST-API-Key":"restApiKey"
           //      },
           //      body: values
           //  }).then(function(res) {
           //      debugger;
           //      console.log("Response succeeded?", JSON.stringify(res.ok));
           //      console.log(JSON.stringify(res));
           //      return ({
           //          status: 'ok',
           //          type,
           //          currentAuthority: 'admin',
           //      });
           //
           //  }).catch(function(e) {
           //      console.log("fetch fail", e);
           //  });
//
            //
            // fetch('http://10.82.33.118:3001/functions/login', {
            //     method: "POST",
            //     mode: "no-cors",
            //     headers: {
            //         "Content-Type": "application/x-www-form-urlencoded"
            //     },
            //     body: values
            // }).then(function(res) {
            //     console.log("Response succeeded?", JSON.stringify(res.ok));
            //     console.log(JSON.stringify(res));
            // }).catch(function(e) {
            //     console.log("fetch fail", JSON.stringify(params));
            // });
            //


        }
    };

    changeAutoLogin = e => {
        this.setState({
            autoLogin: e.target.checked,
        });
    };

    renderMessage = content => (
        <Alert style={{ marginBottom: 24 }} message={content} type="error" showIcon />
    );

    render() {
        const { login, submitting } = this.props;
        const { type, autoLogin } = this.state;
        return (
            <div className={styles.main}>
                <Login
                    defaultActiveKey={type}
                   onTabChange={this.onTabChange}
                    onSubmit={this.handleSubmit}
                    ref={form => {
                        this.loginForm = form;
                    }}
                >
                    <Tab key="account" tab={'用户登录'}>
                        {login.status === 'error' &&login.type === 'account' && !submitting && this.renderMessage('用户名或者密码有误')}
                        <UserName name="userName" placeholder="username: admin or user" />
                        <Password
                            name="password"
                            placeholder="password: 888888"
                            onPressEnter={() => this.loginForm.validateFields(this.handleSubmit)}
                        />
                    </Tab>

                    <div>
                        <Checkbox checked={autoLogin} onChange={this.changeAutoLogin}>
                            自动登录
                        </Checkbox>
                        <a style={{ float: 'right' }} href="">
                           忘记密码
                        </a>
                    </div>
                    <Submit loading={submitting}>
                        登录
                    </Submit>
                    {/*<div className={styles.other}>*/}
                        {/*<Icon type="alipay-circle" className={styles.icon} theme="outlined" />*/}
                        {/*<Icon type="taobao-circle" className={styles.icon} theme="outlined" />*/}
                        {/*<Icon type="weibo-circle" className={styles.icon} theme="outlined" />*/}
                        {/*<Link className={styles.register} to="/user/register">*/}
                            {/*注册账户*/}
                       {/**/}
                        {/*</Link>*/}
                    {/*</div>*/}
                </Login>
            </div>
        );
    }
}

export default LoginPage;
