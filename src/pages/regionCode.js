import { Card, Col, Row, Icon } from 'antd';

const { Meta } = Card;

const cardStyle = {
  marginBottom: '20px',
};

export default () => {
  return (
    <div>
      <Row gutter={24}>
        <Col span={6}>
          <Card
            hoverable
            style={{ width: 240, marginBottom: '20px' }}
            cover={<img src="../images/ho.png" />}
            actions={[<Icon type="edit" />, <Icon type="delete" />, <Icon type="download" />]}
          >
            <Meta title="HO" />
          </Card>
        </Col>
        <Col span={6}>
          <Card
            hoverable
            style={{ width: 240, marginBottom: '20px' }}
            cover={<img src="../images/ho.png" />}
            actions={[<Icon type="edit" />, <Icon type="delete" />, <Icon type="download" />]}
          >
            <Meta title="north" />
          </Card>
        </Col>
        <Col span={6}>
          <Card
            hoverable
            style={{ width: 240, marginBottom: '20px' }}
            cover={<img src="../images/ho.png" />}
            actions={[<Icon type="edit" />, <Icon type="delete" />, <Icon type="download" />]}
          >
            <Meta title="south" />
          </Card>
        </Col>
        <Col span={6}>
          <Card
            hoverable
            style={{ width: 240, marginBottom: '20px' }}
            cover={<img src="../images/ho.png" />}
            actions={[<Icon type="edit" />, <Icon type="delete" />, <Icon type="download" />]}
          >
            <Meta title="west" />
          </Card>
        </Col>
        <Col span={6}>
          <Card
            hoverable
            style={{ width: 240, marginBottom: '20px' }}
            cover={<img src="../images/ho.png" />}
            actions={[<Icon type="edit" />, <Icon type="delete" />, <Icon type="download" />]}
          >
            <Meta title="east" />
          </Card>
        </Col>
      </Row>
    </div>
  );
};
