import React from 'react';
import { Table, Row, Col, Divider, Rate, Button, Icon,  Card, Tabs, Input, DatePicker, Menu, Dropdown } from 'antd';
import { connect } from 'dva';
import {
    G2,
    Chart,
    Geom,
    Axis,
    Tooltip,
    Coord,
    Label,
    Legend,
    View,
    Guide,
    Shape,
    Facet,
    Util
} from "bizcharts";
import DataSet from "@antv/data-set";
import styles from '../layout/index.less';

const mapStateToProps = state => {
    const leaderboardData = state['leaderboard'];
    return {
        leaderboardData
    };
};

//柱形
// const data = [
//     {
//         kolName: "单炯",
//         sales: 1338
//     },
//     {
//         kolName: "孙欣",
//         sales: 1152
//     },
//     {
//         kolName: "张仲尧",
//         sales: 1121
//     },
//     {
//         kolName: "刘文",
//         sales: 1115
//     },
//     {
//         kolName: "李潇潇",
//         sales: 998
//     },
//     {
//         kolName: "王佳敏",
//         sales: 989
//     },
//     {
//         kolName: "麻团儿",
//         sales: 889
//     },
//     {
//         kolName: "张蔚然",
//         sales: 788
//     }
// ];
// const cols = {
//     sales: {
//         tickInterval: 200
//     }
// };

// 饼图
const { DataView } = DataSet;
const { Html } = Guide;
const testData = [
    {
        item: "前测名额",
        count: 180
    },
    {
        item: "后测名额",
        count: 140
    },
    {
        item: "无测试数据",
        count: 1180
    }
];
const dv = new DataView();
dv.source(testData).transform({
    type: "percent",
    field: "count",
    dimension: "item",
    as: "percent"
});
const testCols = {
    percent: {
        formatter: val => {
            val = val* 100
            val = val.toFixed(2) + "%";
            return val;
        }
    }
};
// 表单

//表单 END
@connect(mapStateToProps)
export default class Analysis extends React.Component {

    render() {
        return (
                <div>
                        <h2>实时五星KOL名单</h2>
                        <Divider />
                        <Row gutter={24}>
                            <Col xl={12} lg={24} md={24} sm={24} xs={24}>
                                <Chart
                                   // height={window.innerHeight}
                                    height ={400}
                                    data={dv}
                                    scale={testCols}
                                 //   padding={[10, 10, 8, 8]}
                                    forceFit
                                >
                                    <Coord type={"theta"} radius={0.75} innerRadius={0.6} />
                                    <Axis name="percent" />
                                    <Legend
                                        position="left"
                                        offsetY={-200}
                                        offsetX={5}
                                    />
                                    <Tooltip
                                        showTitle={false}
                                        itemTpl="<li><span style=&quot;background-color:{color};&quot; class=&quot;g2-tooltip-marker&quot;></span>{name}: {value}</li>"
                                    />
                                    <Guide>
                                        <Html
                                            position={["50%", "50%"]}
                                            html="<div style=&quot;color:#8c8c8c;font-size:1.16em;text-align: center;width: 10em;&quot;>KOL总数<br><span style=&quot;color:#262626;font-size:2.5em&quot;>1200</span></div>"
                                            alignX="middle"
                                            alignY="middle"
                                        />
                                    </Guide>
                                    <Geom
                                        type="intervalStack"
                                        position="percent"
                                        color="item"
                                        tooltip={[
                                            "item*percent",
                                            (item, percent) => {
                                                percent = percent* 100
                                                percent = percent.toFixed(2) + "%";
                                                return {
                                                    name: item,
                                                    value: percent
                                                };
                                            }
                                        ]}
                                        style={{
                                            lineWidth: 1,
                                            stroke: "#fff"
                                        }}
                                    >
                                        <Label
                                            content="percent"
                                            formatter={(val, item) => {
                                                return item.point.item + ": " + val;
                                            }}
                                        />
                                    </Geom>
                                </Chart>
                            </Col>
                            <Col xl={12} lg={24} md={24} sm={24} xs={24}>
                                <h3><Divider type="vertical" /><Icon type="pie-chart" /> 其他分析</h3>
                                <ul className={styles.rankingList}>
                                    <li>1245345</li>
                                </ul>
                            </Col>
                        </Row>
                        <Row gutter={24}>
                                    <Col xl={12} lg={12} md={12} sm={24} xs={24}>
                                        <h3><Icon type="weibo" /> 微博平台五星榜</h3>
                                        <Table columns={this.props.leaderboardData.leaderboardColumns} dataSource={this.props.leaderboardData.weiboData} size="small" pagination={false}/>
                                            {/*柱形 <Chart height={300} data={data} scale={cols} >*/}
                                                {/*<Axis name="kolName" />*/}
                                                {/*<Axis name="sales" />*/}
                                                {/*<Tooltip*/}
                                                    {/*crosshairs={{*/}
                                                        {/*type: "y"*/}
                                                    {/*}}*/}
                                                {/*/>*/}
                                                {/*<Geom type="interval" position="kolName*sales" />*/}
                                            {/*</Chart> 柱形*/}
                                    </Col>
                                    <Col xl={12} lg={12} md={12} sm={24} xs={24}>
                                        <h3><Icon type="wechat" /> 微信公众号平台五星榜</h3>
                                        <Table columns={this.props.leaderboardData.leaderboardColumns} dataSource={this.props.leaderboardData.wexinData} size="small" pagination={false}/>
                                        {/*<Chart height={300} data={data} scale={cols} >*/}
                                            {/*<Axis name="kolName" />*/}
                                            {/*<Axis name="sales" />*/}
                                            {/*<Tooltip*/}
                                                {/*crosshairs={{*/}
                                                    {/*type: "y"*/}
                                                {/*}}*/}
                                            {/*/>*/}
                                            {/*<Geom type="interval" position="kolName*sales" />*/}
                                        {/*</Chart>*/}
                                    </Col>
                        </Row>
                        <Row gutter={24}>
                            <Col xl={24} lg={24} md={24} sm={24} xs={24}>
                                <h3 style={{marginTop:15,marginBottom:10}}><Icon type="fund" /> 小红书平台五星榜</h3>
                                <Table columns={this.props.leaderboardData.leaderboardColumns} dataSource={this.props.leaderboardData.xssData} size="small" pagination={false}/>
                            </Col>
                        </Row>
                 </div>

        );
    }

};
