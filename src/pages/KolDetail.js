import React, { Component } from 'react';
import { Table, Breadcrumb, Divider, Icon, Form,  Row, Input, Select, Tabs, Rate, Modal } from 'antd';
import { connect } from 'dva';
import BankInfoimation from './Forms/KolBankInformation.js';
import KolInfoimation from './Forms/KolInformation.js';
import KolPlatformInfoimation from './Forms/KolPlatformInfomation.js';

const FormItem = Form.Item;
const InputGroup = Input.Group;
const Option = Select.Option;
const TabPane = Tabs.TabPane;

const BankInfoimationForm = Form.create()(BankInfoimation);
const KolInfoimationForm = Form.create()(KolInfoimation);
const KolPlatformInfoimationForm = Form.create()(KolPlatformInfoimation);

const namespace = 'kolDetails';
const mapStateToProps = state => {
    const dataDetail = state[namespace];
    return {
      dataDetail
    };
};

@connect(mapStateToProps)
export default class KolDeatil extends Component {
  constructor(props) {
    super(props);
    this.state = {
        qianCeModalVisible: false,
        houCeModalVisible: false,
        thirdPartyTestColumns: [
        {
          fixed: 'left',
          title: '项目编号',
          width: '150',
          dataIndex: 'number',
          key: 'number',
        },
        {
          title: '项目名称',
          dataIndex: 'name',
          key: 'name',
        },
        {
          title: '平台',
          dataIndex: 'platform',
          key: 'platform',
        },
        {
          title: '内容属性',
          key: 'content',
          dataIndex: 'content',
        },
        {
            title: '前测数据',
            key: 'qianCe',
            dataIndex: 'qianCe',
            render: qianCe => {
                if(qianCe>=30){
                    return <span><a href="javascript:void(0);" onClick={this.showQianCeModal}>{qianCe}</a><Icon type="caret-up" style={{color:'#32CD32'}}/></span>
                }else {
                    return <span><a href="javascript:void(0);" onClick={this.showQianCeModal}>{qianCe}</a><Icon type="caret-down" style={{color:'#f00'}} /></span>
                }
            },
        },
        {
            title: '后测数据',
            key: 'houCe',
            dataIndex: 'houCe',
            render: houCe => {
                if(houCe>=30){
                    return <span><a href="javascript:void(0);" onClick={this.showHoCeModal}>{houCe}</a><Icon type="caret-up" style={{color:'#32CD32'}}/></span>
                }else {
                    return <span><a href="javascript:void(0);" onClick={this.showHoCeModal}>{houCe}</a><Icon type="caret-down" style={{color:'#f00'}} /></span>
                }
            },
        },
        {
          title: '日常阅读',
          key: 'readerShip',
          dataIndex: 'readerShip',
        },
        {
          title: '日常互动',
          key: 'engagement',
          dataIndex: 'engagement',
        },
        {
          title: '合作费用',
          key: 'cost',
          dataIndex: 'cost',
        },
        {
          title: 'ROI',
          key: 'roi',
          dataIndex: 'roi',
        },
        {
          title: 'CPC',
          key: 'cpc',
          dataIndex: 'cpc',
        },
        {
          title: 'CPR',
          key: 'cpr',
          dataIndex: 'cpr',
        },
        {
          title: '星级',
          key: 'start',
          dataIndex: 'start',
          width: 200,
          render: start => {
            return <Rate value={start} count={start} />;
          },
        },
      ],

    };
  }
// 前测 后测
    showHoCeModal = () => {
        this.setState({
            houCeModalVisible: true,
        });
    };

  showQianCeModal = () => {
    this.setState({
      qianCeModalVisible: true,
    });
  };

  houCeHandleOk = e => {
    this.setState({
      houCeModalVisible: false,
    });
  };

  qianCeHandleOk = e => {
    this.setState({
      qianCeModalVisible: false,
    });
  };

  houCehHandleCancel = e => {
    this.setState({
      houCeModalVisible: false,
    });
  };

  qianCehHandleCancel = e => {
    this.setState({
      qianCeModalVisible: false,
    });
  };
// 前测 后测 END
  render() {
        const kolTestColumns = this.props.dataDetail.kolTestColumns;
        const kolDetails = this.props.dataDetail.kolDetail;
        return (
          <div>
              <Breadcrumb>
                  <Breadcrumb.Item href="/">
                      <Icon type="home" />
                  </Breadcrumb.Item>
                  <Breadcrumb.Item href="/kol/anyParams">
                      <Icon type="user" />
                      <span>KOL List</span>
                  </Breadcrumb.Item>
                  <Breadcrumb.Item>
                      {kolDetails.user}
                  </Breadcrumb.Item>
              </Breadcrumb>
              <Divider>{kolDetails.user}</Divider>

                <Tabs defaultActiveKey="2">
                  <TabPane tab="银行卡信息" key="1">
                      <BankInfoimationForm dataSource={kolDetails.bankInfomation} ></BankInfoimationForm>
                  </TabPane>
                  <TabPane tab="基本信息" key="2">
                      <KolInfoimationForm dataSource={kolDetails.kolInfomation}></KolInfoimationForm>
                    {/*<Table*/}
                      {/*columns={this.state.tableColumns}*/}
                      {/*dataSource={this.state.tableData}*/}
                      {/*scroll={{ x: '130%' }}*/}
                    {/*/>*/}
                  </TabPane>
                  <TabPane tab="平台信息" key="3">
                      <KolPlatformInfoimationForm dataSource={kolDetails.kolPlatformInfomation}></KolPlatformInfoimationForm>
                  </TabPane>
                  <TabPane tab="第三方检测数据" key="4">
                    <Table
                      columns={this.state.thirdPartyTestColumns}
                      dataSource={kolDetails.thirdPartyTestData}
                      scroll={{ x: '130%' }}
                    />
                  </TabPane>
                </Tabs>
                <Modal
                  title={kolDetails.user+"微博项目前测数据"}
                  visible={this.state.qianCeModalVisible}
                  onOk={this.qianCeHandleOk}
                  onCancel={this.qianCehHandleCancel}
                  footer={null}
                >
                    <Table columns={kolTestColumns} dataSource={kolDetails.testData} showHeader={false} pagination={false} size='small' />
                </Modal>
                <Modal
                  title={kolDetails.user+"微博项目后测数据"}
                  visible={this.state.houCeModalVisible}
                  onOk={this.houCeHandleOk}
                  onCancel={this.houCehHandleCancel}
                  footer={null}
                >
                    <Table columns={kolTestColumns} dataSource={kolDetails.testDataAfter} showHeader={false} pagination={false} size='small'/>
                </Modal>
          </div>
        );
  }
}
