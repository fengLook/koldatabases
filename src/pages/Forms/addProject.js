import React from 'react';
import { Form, Input, DatePicker, Icon, Cascader, Select, Row, Col, Checkbox, Button, AutoComplete } from 'antd';

const FormItem = Form.Item;

export default class AddProject extends React.Component {
    state = {
        confirmDirty: false,
        autoCompleteResult: [],
    };
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
            }
        });
    }
    render() {
        const { getFieldDecorator } = this.props.form;
        const { autoCompleteResult } = this.state;
        const formItemLayout = {
            labelCol: {
                xs: { span: 20 },
                sm: { span: 4 },
            },
            wrapperCol: {
                xs: { span: 4 },
                sm: { span: 20 },
            },
        };
        return (
            <Form onSubmit={this.handleSubmit}>
                <FormItem {...formItemLayout} label='项目名' >
                    {getFieldDecorator('name', {
                        rules: [{ message: 'Please input your area!', whitespace: true }],
                    })(<Input />)}
                </FormItem>
                <FormItem {...formItemLayout} label='开始时间' >
                    <DatePicker style={{ width: '100%' }} onChange={this.onChange} />
                </FormItem>
                <FormItem {...formItemLayout} label='结束时间'>
                    {getFieldDecorator('endTime', {
                        rules: [{  message: 'Please input your endTime!', whitespace: true }],
                    })(<DatePicker style={{ width: '100%' }} onChange={this.onChange} />)}
                </FormItem>
                <FormItem {...formItemLayout} label='参与区域' >
                    {getFieldDecorator('project', {
                        rules: [{ message: 'Please input your kolTag!', whitespace: true }],
                    })(<Input />)}
                </FormItem>
                <FormItem {...formItemLayout} label='备注' >
                    {getFieldDecorator('remarks', {
                        type:'textarea',
                        rules: [{  message: 'Please input your remarks!', whitespace: true }],
                    })(<Input.TextArea />)}
                </FormItem>
            </Form>
        );
    }
}

//const WrappedRegistrationForm = Form.create()(RegistrationForm);
