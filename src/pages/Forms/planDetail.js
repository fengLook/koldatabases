import React from 'react';
import { Form, Modal, Input, Table,Button,Row, Divider, Col, DatePicker,Select } from 'antd';
import KolList from './KolList';
import { connect } from 'dva';
const namespace = 'planDetail';
const mapStateToProps = state => {
    const planDetail = state[namespace];
    return {
        planDetail
    };
};
const FormItem = Form.Item;

const { MonthPicker, RangePicker, WeekPicker } = DatePicker;
const Option = Select.Option;
const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
        console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
    },
    getCheckboxProps: record => ({
        name: record.name,
    }),
};

const CollectionCreateForm = Form.create()(
    class extends React.Component {
        render() {
            const { columns, dataSource, visible, onCancel, onCreate, form } = this.props;
            return (
                <Modal
                    width={window.innerWidth-100}
                    visible={visible}
                    title="添加KOL"
                    okText="确定添加"
                    cancelText="取消"
                    onCancel={onCancel}
                    // footer={null}
                    onOk={onCreate}
                >
                    <KolList isDisabled={false} rowSelection={rowSelection} columns={columns} dataSource={dataSource}></KolList>
                </Modal>
            );
        }
    }
);

@connect(mapStateToProps)
export default class PlanDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            confirmDirty: false,
            autoCompleteResult: [],
            qianCeModalVisible: false,
            houCeModalVisible: false,
            confirmDirty: false,
            isDisabled : true,
        };
    }
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                //   console.log('Received values of form: ', values);
            }
        });
    }
    // 新增 KOL 弹窗相关方法
    saveFormRef = formRef => {
        this.formRef = formRef;
    };
    handleAddKOL = () => {
        this.setState({
            addKOLModalVisible: true,
        });
    };
    handleCreate = e => {
        this.setState({
            addKOLModalVisible: false,
        });
    };
    handleAddKOLCancel = e => {
        this.setState({
            addKOLModalVisible: false,
        });
    };
    // 新增 KOL 弹窗相关方法 END
     handleChange =(value)  => {
        console.log(`selected ${value}`);
    }
    handleEdit  = (e) => {
        this.setState({ isDisabled: false});
    }
    handleCancel = (e) => {
        this.setState({ isDisabled: true });
        this.props.form.resetFields();
    }
    onChange=(date, dateString)=> {
        console.log(date, dateString);
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        const { autoCompleteResult } = this.state;
        const formItemLayout = {
            labelCol: {
                xs: { span: 20 },
                sm: { span: 4 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 },
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 8,
                },
            },
        };
        return (
            <Form onSubmit={this.handleSubmit}>
                <Col span={12} style={{ textAlign: 'right' }}>
                    <FormItem {...formItemLayout} label='计划名' >
                        {getFieldDecorator('name', {
                            initialValue:'1115 selling kit SC',
                            rules: [{ message: 'Please input your name!', whitespace: true }],
                        })(<Input />)}
                    </FormItem>
                </Col>
                <Col span={12} style={{ textAlign: 'right' }}>
                    <FormItem {...formItemLayout} label='所属项目' >
                        {getFieldDecorator('project', {
                            initialValue:'屈臣氏 HWB 项目后测评估',
                            rules: [{ message: 'Please input your project!', whitespace: true }],
                        })(<Input />)}
                    </FormItem>
                </Col>
                <Col span={12} style={{ textAlign: 'right' }}>
                    <FormItem {...formItemLayout} label='付款方式'>
                        {getFieldDecorator('paymentMethod', {
                            rules: [{  message: 'Please input your kolTag!', whitespace: true }],
                        })(
                            <Select style={{ width: '100%' }} initialValue="lucy" onChange={this.handleChange}>
                                <Option value="jack">微创</Option>
                                <Option value="lucy">金源</Option>
                                <Option value="Yiminghe">watsons自付</Option>
                            </Select>
                        )}
                    </FormItem>
                </Col>
                <Col span={12} style={{ textAlign: 'right' }}>
                    <FormItem {...formItemLayout} label='开始时间' >
                       <DatePicker style={{ width: '100%' }} onChange={this.onChange} />
                    </FormItem>
                </Col>
                <Col span={12} style={{ textAlign: 'right' }}>
                    <FormItem {...formItemLayout} label='备注'>
                        {getFieldDecorator('remarks', {
                            initialValue:'test Demo',
                            rules: [{  message: 'Please input your remarks!', whitespace: true }],
                        })(<Input.TextArea />)}
                    </FormItem>
                </Col>
                <Col span={12} style={{ textAlign: 'right' }}>
                    <FormItem {...formItemLayout} label='结束时间'>
                        {getFieldDecorator('endTime', {
                            rules: [{  message: 'Please input your endTime!', whitespace: true }],
                        })(<DatePicker style={{ width: '100%' }} onChange={this.onChange} />)}
                    </FormItem>
                </Col>
                <Divider>KOL列表</Divider>
                <p>
                    <Button style={{margin: '0 0 0 10px'}} type="primary" onClick={this.handleAddKOL}>添加KOL</Button>
                </p>
                <Table style={{width:'100%',margin:'0 auto'}} columns={this.props.planDetail.planDetailColumns} dataSource={this.props.planDetail.data}  scroll={{ x: 3500 }}/>
                <FormItem {...tailFormItemLayout}>
                    <Button type="primary" onClick={this.handleEdit} hidden={!this.state.isDisabled}>编辑</Button>
                    <Button type="primary" htmlType="submit" hidden={this.state.isDisabled}>提交保存</Button>&nbsp;
                    <Button onClick={this.handleCancel} hidden={this.state.isDisabled}>取消</Button>
                </FormItem>
                <CollectionCreateForm
                    columns={this.props.columns}
                    dataSource={this.props.dataSource}
                    wrappedComponentRef={this.saveFormRef}
                    visible={this.state.addKOLModalVisible}
                    onCancel={this.handleAddKOLCancel}
                    onCreate={this.handleCreate}
                />
            </Form>
        );
    }
}

//const WrappedRegistrationForm = Form.create()(RegistrationForm);
