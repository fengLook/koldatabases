import React from 'react';
import { Button, Divider, Form, Table, Row, Col, Input, Select} from 'antd';

const InputGroup = Input.Group;
function handleChange(value) {
    console.log(`selected ${value}`);
}

export default class KolList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isDisabled: props.isDisabled,
            dataSource: props.dataSource,
            columns: props.columns,
            rowSelection: props.rowSelection
        };
    }
    render() {
        return (
            <div>
                        <Form layout="inline"  hidden={this.state.isDisabled}>
                            <Row gutter={24} style={{ marginBottom: '10px' }}>
                                <Col span={6}>
                                    <div>
                                        <Input addonBefore="平台账户ID" />
                                    </div>
                                </Col>
                                <Col span={6}>
                                    <div>
                                        <Input addonBefore="平台账户名 " />
                                    </div>
                                </Col>
                                <Col span={6}>
                                    <div>
                                        <Input addonBefore="所在地 " />
                                    </div>
                                </Col>
                                <Col span={6} gutter={24}>
                                    <span style={{textAlign:'left'}}>区域：</span>
                                    <Select addonBefore="区域 " defaultValue="ho" style={{ width: 120 }} onChange={handleChange}>
                                        <Option value="ho">ho</Option>
                                        <Option value="north">north</Option>
                                        {/*<Option value="disabled" disabled>Disabled</Option>*/}
                                        <Option value="south">south</Option>
                                        <Option value="west">west</Option>
                                        <Option value="east">east</Option>
                                    </Select>
                                </Col>
                            </Row>
                            <Row gutter={24} style={{ marginBottom: '16px' }}>
                                <Col span={6} >
                                    <div style={{marginLeft: 58 }} >
                                        {/*<Input addonBefore="粉丝数 " />*/}
                                        {/*<Input addonBefore="粉丝数 " style={{ width: 30, borderLeft: 0, pointerEvents: 'none', backgroundColor: '#fff' }} placeholder="~" disabled />*/}
                                        <span style={{ display: 'block', textAlign:'left',  float: 'left',marginTop: 5, marginLeft: -58,
                                            position: 'relative',
                                            width: 58, borderLeft: 0, pointerEvents: 'none', backgroundColor: '#fff' }} >粉丝数：</span>
                                        <InputGroup compact gutter={24}>
                                        <Input style={{ width: '40%',textAlign: 'center' }} placeholder="Minimum" />
                                        <Input style={{ width: '20%',borderLeft: 0, pointerEvents: 'none', backgroundColor: '#fff' }} placeholder="~" disabled />
                                        <Input  style={{ width: '40%',textAlign: 'center', borderLeft: 0 }} placeholder="Maximum" />
                                        </InputGroup>
                                    </div>
                                </Col>
                                <Col span={6}>
                                    <div>
                                        <Input addonBefore="费用大于 " />
                                    </div>
                                </Col>
                                <Col span={6}>
                                    <div>
                                        <Input addonBefore="费用小于 " />
                                    </div>
                                </Col>
                                <Col span={4}>
                                    <div>
                                        <Input addonBefore="Top vaule Lol " />
                                    </div>
                                </Col>
                                <Col span={2} style={{ textAlign: 'left', paddingLeft: '0' }}>
                                    <Button type="primary">搜索</Button>
                                </Col>
                            </Row>
                        </Form>
                        <Divider hidden={this.state.isDisabled} ></Divider>
                        <Table
                            rowSelection={this.state.rowSelection}
                            columns={this.state.columns}
                            dataSource={this.state.dataSource}
                        />
            </div>
        )}
}
