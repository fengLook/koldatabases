import React from 'react';
import { Form, Input, DatePicker, Icon, Modal, Select, Row, Col, Divider, Button, AutoComplete } from 'antd';
import KolList from './KolList';
import { connect } from 'dva';

const mapStateToProps = state => {
    const kols = state.kols;
    return kols;
};
const Option = Select.Option;
const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
        console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
    },
    getCheckboxProps: record => ({
        name: record.name,
    }),
};
const FormItem = Form.Item;
const CollectionCreateForm = Form.create()(
    class extends React.Component {
        render() {
            const { columns, dataSource, visible, onCancel, onCreate, form } = this.props;
            return (
                <Modal
                        width={window.innerWidth-100}
                        visible={visible}
                        title="添加KOL"
                        okText="确定添加"
                        cancelText="取消"
                        onCancel={onCancel}
                        onOk={onCreate}
                    // footer={null}
                >
                    <KolList isDisabled={false} rowSelection={rowSelection} columns={columns} dataSource={dataSource}></KolList>
                    {/*<Row style={{ marginBottom: '10px' }}>*/}
                    {/*<Button type="primary" block>确定添加</Button>*/}
                    {/*</Row>*/}
                </Modal>
            );
        }
    }
);
function handleChange(value) {
    // console.log(`selected ${value}`);
}
@connect(mapStateToProps)
export default class AddPlan extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            confirmDirty: false,
            autoCompleteResult: [],
            qianCeModalVisible: false,
            houCeModalVisible: false,
            confirmDirty: false,
            isDisabled : true
        };
    }
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
            }
        });
    }
    // 新增 KOL 弹窗相关方法
    saveFormRef = formRef => {
        this.formRef = formRef;
    };
    handleAddKOL = () => {
       // console.log('ddd');
       this.setState({
           newPlanModalVisible: true,
       });
    };
    handleCreate = e => {
        this.setState({
            newPlanModalVisible: false,
        });
    };
    handleAddKOLCancel = e => {
        this.setState({
            newPlanModalVisible: false,
        });
    };
    // 新增 KOL 弹窗相关方法 END
    render() {
        const { getFieldDecorator } = this.props.form;
        const { autoCompleteResult } = this.state;
        const formItemLayout = {
            labelCol: {
                xs: { span: 20 },
                sm: { span: 4 },
            },
            wrapperCol: {
                xs: { span: 4 },
                sm: { span: 20 },
            },
        };
        return (
            <Form onSubmit={this.handleSubmit}>
                <FormItem {...formItemLayout} label='计划名' >
                    {getFieldDecorator('name', {
                        rules: [{ message: 'Please input your area!', whitespace: true }],
                    })(<Input />)}
                </FormItem>
                <FormItem {...formItemLayout} label='所属项目' >
                    {getFieldDecorator('project', {
                        initialValue:'1115selling kit的计划',
                        rules: [{ message: 'Please input your kolTag!', whitespace: true }],
                    })(<Input disabled={true}/>)}
                </FormItem>
                <FormItem {...formItemLayout} label='付款方式'>
                    {getFieldDecorator('paymentMethod', {
                        rules: [{  message: 'Please input your kolTag!', whitespace: true }],
                    })(
                        <Select style={{ width: '100%' }} initialValue="lucy" onChange={this.handleChange}>
                            <Option value="jack">微创</Option>
                            <Option value="lucy">金源</Option>
                            <Option value="Yiminghe">watsons自付</Option>
                        </Select>
                    )}
                </FormItem>
                <FormItem {...formItemLayout} label='开始时间' >
                    <DatePicker style={{ width: '100%' }} onChange={this.onChange} />
                </FormItem>
                <FormItem {...formItemLayout} label='结束时间'>
                    {getFieldDecorator('endTime', {
                       // rules: [{  message: 'Please input your endTime!', whitespace: true }],
                    })(<DatePicker style={{ width: '100%' }} onChange={this.onChange} />)}
                </FormItem>
                <FormItem {...formItemLayout} label='备注' >
                    {getFieldDecorator('remarks', {
                        rules: [{  message: 'Please input your remarks!', whitespace: true }],
                    })(<Input.TextArea />)}
                </FormItem>
                <Button style={{background: '#48D1CC', color:'#fff'}} block={true} onClick={this.handleAddKOL}><Icon type="plus-square" />添加KOL</Button>
                <CollectionCreateForm
                    columns={this.props.popAddKolColumns}
                    dataSource={this.props.kolList}
                    wrappedComponentRef={this.saveFormRef}
                    visible={this.state.newPlanModalVisible}
                    onCancel={this.handleAddKOLCancel}
                    onCreate={this.handleCreate}
                />
            </Form>
        );
    }
}

//const WrappedRegistrationForm = Form.create()(RegistrationForm);
