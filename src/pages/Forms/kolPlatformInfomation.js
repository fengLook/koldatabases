import React from 'react';
import { Form, Input, Tooltip, Icon, Cascader, Select, Row, Col, Checkbox, Button, AutoComplete } from 'antd';
import styles from './form.less';
const FormItem = Form.Item;

export default class KolPlatformInfoimation extends React.Component {
    state = {
        confirmDirty: false,
        autoCompleteResult: [],
        isDisabled : true,
        dataSource: this.props.dataSource
    };
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
            }
        });
    }
    handleEdit  = (e) => {
        this.setState({ isDisabled: false});
    }
    handleCancel = (e) => {
        this.setState({ previewVisible: false,isDisabled: true });
        this.props.form.resetFields();
    }
    render() {
        const { getFieldDecorator } = this.props.form;
        const { autoCompleteResult } = this.state;
        const formItemLayout = {
            labelCol: {
                xs: { span: 20 },
                sm: { span: 4 },
            },
            wrapperCol: {
                xs: { span: 4 },
                sm: { span: 20 },
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 8,
                },
            },
        };
        return (
            <Form onSubmit={this.handleSubmit}>
                <Row>
                    <Col span={12} style={{ textAlign: 'right' }}>
                        <FormItem {...formItemLayout} label='媒体平台' >
                            {getFieldDecorator('mediaPlatform', {
                                initialValue:this.state.dataSource.mediaPlatform,
                                rules: [{ message: 'Please input your name!',  whitespace: true }],
                            })(<Input className={styles.inputColorDeep} disabled={this.state.isDisabled} />)}
                        </FormItem>
                    </Col>
                     <Col span={12} style={{ textAlign: 'right' }}>
                        <FormItem {...formItemLayout} label='标签' >
                            {getFieldDecorator('label', {
                                initialValue:this.state.dataSource.label,
                                rules: [{ message: 'Please input your label!', whitespace: true }],
                            })(<Input className={styles.inputColorDeep} disabled={this.state.isDisabled} />)}
                        </FormItem>
                </Col>
                    <Col span={12} style={{ textAlign: 'right' }}>
                        <FormItem {...formItemLayout} label='账户ID' >
                            {getFieldDecorator('id', {
                                initialValue:this.state.dataSource.id,
                                rules: [{  message: 'Please input your id!', whitespace: true }],
                            })(<Input className={styles.inputColorDeep} disabled={this.state.isDisabled} />)}
                        </FormItem>
                    </Col>
                    <Col span={12} style={{ textAlign: 'right' }}>
                        <FormItem {...formItemLayout} label='粉丝数' >
                            {getFieldDecorator('address', {
                                initialValue:this.state.dataSource.numberOfFans,
                                rules: [{  message: 'Please input your address!', whitespace: true }],
                            })(<Input className={styles.inputColorDeep} disabled={this.state.isDisabled} />)}
                        </FormItem>
                    </Col>
                    <Col span={12} style={{ textAlign: 'right' }}>
                        <FormItem {...formItemLayout} label='平均阅读量' >
                            {getFieldDecorator('averageReading', {
                                initialValue:this.state.dataSource.averageReading,
                                rules: [{  message: 'Please input your qq!', whitespace: true }],
                            })(<Input className={styles.inputColorDeep} disabled={this.state.isDisabled} />)}
                        </FormItem>
                    </Col>
                    <Col span={12} style={{ textAlign: 'right' }}>
                        <FormItem {...formItemLayout} label='平均互动量' >
                            {getFieldDecorator('averageInteraction', {
                                initialValue:this.state.dataSource.averageInteraction,
                                rules: [{  message: 'Please input your wxine!', whitespace: true }],
                            })(<Input className={styles.inputColorDeep} disabled={this.state.isDisabled} />)}
                        </FormItem>
                    </Col>
                    <Col span={12} style={{ textAlign: 'right' }}>
                        <FormItem {...formItemLayout} label='所在地' >
                            {getFieldDecorator('location', {
                                initialValue:this.state.dataSource.location,
                                rules: [{  message: 'Please input your location!', whitespace: true }],
                            })(<Input className={styles.inputColorDeep} disabled={this.state.isDisabled} />)}
                        </FormItem>
                    </Col>
                    <Col span={12} style={{ textAlign: 'right' }}>
                        <FormItem {...formItemLayout} label='合作品牌' >
                            {getFieldDecorator('cooperationBrand', {
                                initialValue:this.state.dataSource.cooperationBrand,
                                rules: [{  message: 'Please input your photos!', whitespace: true }],
                            })(<Input className={styles.inputColorDeep} disabled={this.state.isDisabled} />)}
                        </FormItem>
                    </Col>
                    <Col span={12} style={{ textAlign: 'right' }}>
                        <FormItem {...formItemLayout} label='所属区域' >
                            {getFieldDecorator('area', {
                                initialValue:this.state.dataSource.area,
                                rules: [{  message: 'Please input your area!', whitespace: true }],
                            })(<Input className={styles.inputColorDeep} disabled={this.state.isDisabled} />)}
                        </FormItem>
                    </Col>
                    <Col span={12} style={{ textAlign: 'right' }}>
                        <FormItem {...formItemLayout} label='联系人' >
                            {getFieldDecorator('contact', {
                                initialValue:this.state.dataSource.contact,
                                rules: [{  message: 'Please input your photos!', whitespace: true }],
                            })(<Input className={styles.inputColorDeep} disabled={this.state.isDisabled} />)}
                        </FormItem>
                    </Col>
                    <Col span={12} style={{ textAlign: 'right' }}>
                        <FormItem {...formItemLayout} label='审核备注' >
                            {getFieldDecorator('auditNote', {
                                initialValue:this.state.dataSource.auditNote,
                                rules: [{  message: 'Please input your audit note!', whitespace: true }],
                            })(<Input.TextArea disabled={this.state.isDisabled} className={styles.inputColorDeep} />)}
                        </FormItem>
                    </Col>
                    <Col span={12} style={{ textAlign: 'right' }}>
                        <FormItem {...formItemLayout} label='备注' >
                            {getFieldDecorator('remarks', {
                                initialValue:this.state.dataSource.remarks,
                                rules: [{  message: 'Please input your remarks!', whitespace: true }],
                            })(<Input.TextArea disabled={this.state.isDisabled} className={styles.inputColorDeep} />)}
                        </FormItem>
                    </Col>
                </Row>
                <FormItem {...tailFormItemLayout}>
                    <Button type="primary" onClick={this.handleEdit} hidden={!this.state.isDisabled}>编辑</Button>
                    <Button type="primary" htmlType="submit" hidden={this.state.isDisabled}>提交保存</Button>&nbsp;
                    <Button onClick={this.handleCancel} hidden={this.state.isDisabled}>取消</Button>
                </FormItem>
            </Form>
        );
    }
}

