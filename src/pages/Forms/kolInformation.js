import { Form, Upload, Modal, Input, Icon, Select, Button, AutoComplete } from 'antd';
import styles from './form.less';
const FormItem = Form.Item;
const Option = Select.Option;
const AutoCompleteOption = AutoComplete.Option;

export default class KolInfoimation extends React.Component {
    state = {
        confirmDirty: false,
        autoCompleteResult: [],
        previewVisible: false,
        previewImage: '',
        isDisabled : true,
        dataSource: this.props.dataSource
    };
    handleEdit  = (e) => {
        this.setState({ isDisabled: false});
    }
    handleCancel = (e) => {
        this.setState({
            previewVisible: false,
            isDisabled: true
        });
        this.props.form.resetFields();
    }
    //照片相关
    handlePreview = (file) => {
        this.setState({
            previewImage: file.url || file.thumbUrl,
            previewVisible: true,
        });
    }
    handleChange = ({ fileList }) => this.setState({ fileList })
    // 照片相关 END
    //表单提交
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
            }
        });
    }

    handleConfirmBlur = (e) => {
        const value = e.target.value;
        this.setState({ confirmDirty: this.state.confirmDirty || !!value });
    }

    compareToFirstPassword = (rule, value, callback) => {
        const form = this.props.form;
        if (value && value !== form.getFieldValue('password')) {
            callback('Two passwords that you enter is inconsistent!');
        } else {
            callback();
        }
    }

    validateToNextPassword = (rule, value, callback) => {
        const form = this.props.form;
        if (value && this.state.confirmDirty) {
            form.validateFields(['confirm'], { force: true });
        }
        callback();
    }

    handleWebsiteChange = (value) => {
        let autoCompleteResult;
        if (!value) {
            autoCompleteResult = [];
        } else {
            autoCompleteResult = ['.com', '.org', '.net'].map(domain => `${value}${domain}`);
        }
        this.setState({ autoCompleteResult });
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        const { autoCompleteResult } = this.state;
        const formItemLayout = {
            labelCol: {
                xs: { span: 22 },
                sm: { span: 2 },
            },
            wrapperCol: {
                xs: { span: 4 },
                sm: { span: 20 },
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 8,
                },
            },
        };
        const prefixSelector = getFieldDecorator('prefix', {
            initialValue: '86',
        })(
            <Select style={{ width: 70 }}>
                <Option value="86">+86</Option>
                <Option value="87">+87</Option>
            </Select>
        );
        const websiteOptions = autoCompleteResult.map(website => (
            <AutoCompleteOption key={website}>{website}</AutoCompleteOption>
        ));
        const { previewVisible, previewImage} = this.state;
        const fileList = this.props.dataSource.fileList;
        const uploadButton = (
            <div>
                <Icon type="plus" />
                <div className="ant-upload-text">Upload</div>
            </div>
        );
        return (
            <Form onSubmit={this.handleSubmit}>
                <FormItem {...formItemLayout} label='名字' >
                    {getFieldDecorator('name', {
                        initialValue:this.state.dataSource.name,
                        rules: [
                            { message: 'Please input your name!', whitespace: true
                            },
                            {
                                required: true, message: 'Please input your name!',
                            }],
                    })(<Input className={styles.inputColorDeep} disabled={this.state.isDisabled} />)}
                </FormItem>
                <FormItem {...formItemLayout} label='电话' >
                    {getFieldDecorator('phone', {
                        initialValue:this.state.dataSource.phone,
                        rules: [
                            { message: 'Please input your phone!', whitespace: true
                            },
                            {
                            required: true, message: 'Please input your phone!',
                        }],
                    })(<Input className={styles.inputColorDeep} disabled={this.state.isDisabled} />)}
                </FormItem>
                <FormItem{...formItemLayout} label="E-mail">
                    {getFieldDecorator('email', {
                        initialValue:this.state.dataSource.email,
                        rules: [{
                            type: 'email', message: 'The input is not valid E-mail!',
                        }, {
                            required: true, message: 'Please input your E-mail!',
                        }],
                    })(
                        <Input className={styles.inputColorDeep} disabled={this.state.isDisabled} />
                    )}
                </FormItem>
                <FormItem {...formItemLayout} label='地址' >
                    {getFieldDecorator('address', {
                        initialValue:this.state.dataSource.address,
                        rules: [{ message: 'Please input your address!', whitespace: true }],
                    })(<Input className={styles.inputColorDeep} disabled={this.state.isDisabled} />)}
                </FormItem>
                <FormItem {...formItemLayout} label='QQ' >
                    {getFieldDecorator('qq', {
                        initialValue:this.state.dataSource.qq,
                        rules: [{ message: 'Please input your address!', whitespace: true }],
                    })(<Input className={styles.inputColorDeep} disabled={this.state.isDisabled} />)}
                </FormItem>
                <FormItem {...formItemLayout} label='weChat' >
                    {getFieldDecorator('weChat', {
                        initialValue:this.state.dataSource.weChat,
                        rules: [{ message: 'Please input your weChat!', whitespace: true }],
                    })(<Input className={styles.inputColorDeep} disabled={this.state.isDisabled} />)}
                </FormItem>
                <FormItem {...formItemLayout} label='照片' >
                    {getFieldDecorator('fileList', {
                        rules: [{ message: 'Please input your photo!', whitespace: true }],
                    })(<div className="clearfix">
                        <Upload
                            action="//jsonplaceholder.typicode.com/posts/"
                            listType="picture-card"
                            fileList={fileList}
                            onPreview={this.handlePreview}
                            onChange={this.handleChange}
                        >
                            {fileList.length >= 3||this.state.isDisabled ? null : uploadButton}
                        </Upload>
                        <Modal visible={previewVisible} footer={null} onCancel={this.handleCancel}>
                            <img alt="example" style={{ width: '100%' }} src={previewImage} />
                        </Modal>
                    </div>)}
                </FormItem>
                {/*<FormItem {...tailFormItemLayout}>*/}
                    {/*{getFieldDecorator('agreement', {*/}
                        {/*valuePropName: 'checked',*/}
                    {/*})(*/}
                        {/*<Checkbox>I have read the <a href="">agreement</a></Checkbox>*/}
                    {/*)}*/}
                {/*</FormItem>*/}
                <FormItem {...tailFormItemLayout}>
                    <Button type="primary" onClick={this.handleEdit} hidden={!this.state.isDisabled} >编辑</Button>
                    <Button type="primary" htmlType="submit" hidden={this.state.isDisabled} >提交保存</Button>&nbsp;
                    <Button onClick={this.handleCancel} hidden={this.state.isDisabled} >取消</Button>
                </FormItem>
            </Form>
        );
    }
}
