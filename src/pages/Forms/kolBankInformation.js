import React from 'react';
import { Form, Input, Modal, Icon, Cascader, Select, Row, Col, Checkbox, Button, AutoComplete } from 'antd';
import styles from './form.less';
const FormItem = Form.Item;

export default class BankInfoimation extends React.Component {
    state = {
        confirmDirty: false,
        isDisabled : true,
        dataSource: this.props.dataSource,
        loading: false,
        visible: false,
    };
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
             //   console.log('Received values of form: ', values);
            }
        });
    }
    handleEdit  = (e) => {
        this.setState({ isDisabled: false});
    }
    handleCancel = (e) => {
        this.setState({ isDisabled: true });
        this.props.form.resetFields();
    }
    //图像输出 对话框
    showPopImagesModal = () => {
        this.setState({
            visible: true,
        });
    }

    handlePopCancel = () => {
        this.setState({ visible: false });
    }
    //图像输出 对话框 END
    render() {
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol: {
                xs: { span: 22 },
                sm: { span: 2 },
            },
            wrapperCol: {
                xs: { span: 4 },
                sm: { span: 20 },
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 8,
                },
            },
        };
        return (
            <Form onSubmit={this.handleSubmit} >
                <FormItem {...formItemLayout} label='姓名' >
                    {getFieldDecorator('name', {
                        initialValue:this.state.dataSource.name,
                        rules: [{ whitespace: true }],
                    })(<Input className={styles.inputColorDeep} disabled={this.state.isDisabled} />)}
                </FormItem>
                <FormItem {...formItemLayout} label='银行卡账户名' >
                    {getFieldDecorator('bankCardAccountName', {
                        initialValue:this.state.dataSource.bankCardAccountName,
                        rules: [{ whitespace: true }],
                    })(<Input className={styles.inputColorDeep} disabled={this.state.isDisabled} />)}
                </FormItem>
                <FormItem {...formItemLayout} label='银行名称' >
                    {getFieldDecorator('bankName', {
                        initialValue:this.state.dataSource.bankName,
                        rules: [{ whitespace: true }],
                    })(<Input className={styles.inputColorDeep} disabled={this.state.isDisabled}/>)}
                </FormItem>
                <FormItem {...formItemLayout} label='开户行' >
                    {getFieldDecorator('bankAccountOpeningBank', {
                        initialValue:this.state.dataSource.bankAccountOpeningBank,
                        rules: [{ whitespace: true }],
                    })(<Input className={styles.inputColorDeep} disabled={this.state.isDisabled}/>)}
                </FormItem>
                <FormItem {...formItemLayout} label='银行卡号' >
                    {getFieldDecorator('bankCardNumber', {
                        initialValue:this.state.dataSource.bankCardNumber,
                        rules: [{ whitespace: true }],
                    })(<Input className={styles.inputColorDeep} disabled={this.state.isDisabled}/>)}
                </FormItem>
                <FormItem {...formItemLayout} label='联系电话' >
                    {getFieldDecorator('contactNumber', {
                        initialValue:this.state.dataSource.contactNumber,
                        rules: [{ whitespace: true }],
                    })(<Input className={styles.inputColorDeep} disabled={this.state.isDisabled}/>)}
                </FormItem>
                <FormItem {...formItemLayout} label='身份证号码' >
                    {getFieldDecorator('idCard', {
                        initialValue:this.state.dataSource.idCard,
                        rules: [{ whitespace: true }],
                    })(<Input className={styles.inputColorDeep} disabled={this.state.isDisabled}/>)}
                </FormItem>
                <FormItem {...formItemLayout} label='身份证照片' >
                    {/*{getFieldDecorator('icCardPhoto', {*/}
                        {/*initialValue:this.state.dataSource.icCardPhoto,*/}
                        {/*rules: [{ whitespace: true }],*/}
                    {/*})(<Input disabled={this.state.isDisabled}/>)}*/}
                        <img className={styles.tableImage} src={this.state.dataSource.icCardPhoto} onClick={this.showPopImagesModal} title="点击查看大图" />
                </FormItem>
                <FormItem {...tailFormItemLayout}>
                    <Button type="primary" onClick={this.handleEdit} hidden={!this.state.isDisabled}>编辑</Button>
                    <Button type="primary" htmlType="submit" hidden={this.state.isDisabled}>提交保存</Button>&nbsp;
                    <Button onClick={this.handleCancel} hidden={this.state.isDisabled}>取消</Button>
                </FormItem>
                <Modal
                    width={window.innerWidth/2 -10}
                    visible={this.state.visible}
                    title="身份证大图"
                    onCancel={this.handlePopCancel}
                    footer={null}
                >
                    <p><img className={styles.tablePopImage} src={this.state.dataSource.icCardPhoto} /></p>
                </Modal>
            </Form>
        );

    }
}

