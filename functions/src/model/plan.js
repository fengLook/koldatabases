import { Button, Divider } from 'antd';
import Link from 'umi/link';
export default {
    namespace: 'plans',
    state: {
        projectTablecColumns:[
            // {
            //     title: '项目名',
            //     dataIndex: 'name',
            //     key: 'name',
            // },
            {
                title: '发起区域',
                dataIndex: 'region',
                key: 'area',
            },
            {
                title: '预算',
                dataIndex: 'budget',
                key: 'budget',
            },
            {
                title: '开始时间',
                dataIndex: 'startTime',
                key: 'startTime',
            },
            {
                title: '结束时间',
                dataIndex: 'endTime',
                key: 'endTime',
            },],
        projectTablecData:[
            {
                key: '1',
                name: '1115selling kit',
                region: 'HO',
                budget: '1,000',
                startTime: '2018/11/15',
                endTime: '2018/12/18',
            },
        ],
        planTablecData: [
                                    {
                                        key: '1',
                                     //   itemId:'DED12155060464654',
                                        name:'1115 selling kit SC',
                                        region: 'Ho',
                                        budget: '1,000',
                                        startTime: '2018-11-13',
                                        endTime: '2018-12-13',
                                        tag: 'tag',
                                        headquartersReview:'未审核',
                                        note: '不通过原因',
                                    },  {
                                        key: '2',
                                      //  itemId:'GFD12155060464654',
                                        name:'1115 selling kit EC',
                                        region: 'Ho',
                                        budget: '1,000',
                                        startTime: '2018-11-13',
                                        endTime: '2018-12-13',
                                        tag: 'tag',
                                        headquartersReview:'未审核',
                                        note: '不通过原因',
                                    },
                                    // {
                                    //     key: '2',
                                    //     region: 'Ho',
                                    //     local: '广州',
                                    //     budget: '1,000',
                                    //     platform: '微博',
                                    //     userName: '六翼天使',
                                    //     id: 'wuyude333',
                                    //     tag: 'tag',
                                    //     fans: '10,000,0',
                                    //     redingCount: '3000',
                                    //     interactiveCount: '2300',
                                    //     preReading: '3300',
                                    //     preInteractiveCount: '2000',
                                    //     cost: '10,000',
                                    //     PersonalIncomeTax: '800',
                                    //     expectCount: '3200',
                                    //     attribute: '带货',
                                    //     note: '备注',
                                    // },
                        ],
        planTablecColumns:[
            // {
            //     title: '项目编号',
            //     dataIndex: 'itemId',
            //     key: 'itemId',
            //     editable: true,
            // },
            {
                title: '计划名',
                dataIndex: 'name',
                key: 'name',
                editable: true,
                render: text => <Link to="/project/planDetail/anyParams">{text}</Link>,
            },
            {
                title: '开始时间',
                dataIndex: 'startTime',
                key: 'startTime',
                editable: true,
            },
            {
                title: '结束时间',
                dataIndex: 'endTime',
                key: 'endTime',
                editable: true,
            },
            // {
            //     title: 'ID',
            //     dataIndex: 'id',
            //     key: 'id',
            //     editable: true,
            // },
            {
                title: '总部审核情况',
                dataIndex: 'headquartersReview',
                key: 'headquartersReview',
                editable: true,
            },
            // {
            //     title: '合作费用',
            //     dataIndex: 'cost',
            //     key: 'cost',
            //     editable: true,
            // },
            // {
            //     title: '预估个人所得税',
            //     dataIndex: 'PersonalIncomeTax',
            //     key: 'PersonalIncomeTax',
            //     editable: true,
            // },
            // {
            //     title: '预估阅读量',
            //     dataIndex: 'expectCount',
            //     key: 'expectCount',
            //     editable: true,
            // },
            // {
            //     title: '发稿属性',
            //     dataIndex: 'attribute',
            //     key: 'attribute',
            //     editable: true,
            // },
            {
                title: '备注',
                dataIndex: 'note',
                key: 'note',
                width: 200,
                editable: true,
            },
            // {
            //     title: '操作',
            //     dataIndex: 'operation',
            //     fixed: 'right',
            //     width: 200,
                //
                // render: (text, record) => {
                //    const editable = this.isEditing(record);
                //    return (
                //         <div>
                //             {editable ? (
                //                 <span>
                //   <EditableContext.Consumer>
                //     {form => (
                //         <a
                //             href="javascript:;"
                //             onClick={() => this.save(form, record.key)}
                //             style={{ marginRight: 8 }}
                //         >
                //             <Icon type="save" theme="outlined" />
                //             <span style={{ marginLeft: '6px' }}>保存</span>
                //         </a>
                //     )}
                //   </EditableContext.Consumer>
                //   <Divider type="vertical" />
                //   <a href="javascript:;" onClick={() => this.cancel(record.key)}>
                //     <Icon type="close" theme="outlined" />
                //     <span style={{ marginLeft: '6px' }}>取消</span>
                //   </a>
                // </span>
                //             ) : (
                //                 <span>
                //   <a onClick={() => this.edit(record.key)}>
                //     <Icon type="edit" theme="outlined" />
                //     <span style={{ marginLeft: '6px' }}>编辑</span>
                //   </a>
                //   <Divider type="vertical" />
                //   <a>
                //     <Icon type="export" theme="outlined" />
                //     <span style={{ marginLeft: '6px' }}>导出Excel</span>
                //   </a>
                // </span>
                //             )}
                //         </div>
                //     );
                // },
                //
          //  }
        ],
        // planTableChildrenColumns : [
        //     { title: '编号', dataIndex: 'id', key: 'id' },
        //     { title: '所属区域', dataIndex: 'area', key: 'area' },
        //     { title: '所在城市',dataIndex: 'city', key: 'city'},
        //     { title: '平台', dataIndex: 'platform', key: 'platform' },
        //     { title:'账户名', dataIndex: 'name', key:'name'},
        //     { title:'账户ID或主页链接',dataIndex: 'link', key:'link'},
        //     { title: 'KOL标签', dataIndex: 'kolTag', key: 'kolTag'},
        //     { title:'粉丝数', dataIndex: 'fansCount', key:'fansCount'},
        //     { title:'级别', dataIndex: 'level', key:'level'},
        //     { title: '过往合作费用', dataIndex: 'pastCooperationCosts', key: 'pastCooperationCosts'},
        //     { title: '平均阅读量', dataIndex: 'averageReading', key: 'averageReading' },
        //     { title:'与屈臣氏上次合作阅读量', dataIndex: 'lastReadingWithWatsons', key:'lastReadingWithWatsons'},
        //     { title:'平均互动数', dataIndex: 'averageInteractionNumber', key:'averageInteractionNumber'},
        //     { title:'与屈臣氏上次合作互动数', dataIndex: 'lastInteractionWithWatsons', key:'lastInteractionWithWatsons'}
        // ],
        // planTableChildrenData:[
        //     {
        //         key: 1,
        //         id: '1',
        //         area:'华南',
        //         date: '2014-12-24 23:12:00',
        //         city: '广州',
        //         platform: '微博',
        //         name:'无羽的天使',
        //         link:'22',
        //         kolTag:'23`',
        //         fansAmount:'2334',
        //         level:'12444',
        //         pastCooperationCosts:'42233',
        //         averageReading:'12344',
        //         lastReadingWithWatsons:'23443551',
        //         averageInteractionNumber:'2345111',
        //         lastInteractionWithWatsons:'5345254545'
        //     },
        //     {   key: 2,
        //         id: '2',
        //         area:'华南',
        //         date: '2014-12-24 23:12:00',
        //         city: '广州',
        //         platform: '微博',
        //         name:'无羽的天使',
        //         link:'22',
        //         kolTag:'23`',
        //         fansAmount:'2334',
        //         level:'12444',
        //         pastCooperationCosts:'42233',
        //         averageReading:'12344',
        //         lastReadingWithWatsons:'23443551',
        //         averageInteractionNumber:'2345111',
        //         lastInteractionWithWatsons:'5345254545'
        //     }
        // ]
    }

}