import { Button, Divider } from 'antd';
import Link from 'umi/link';

export default {
    namespace: 'kolDetails',
    state: {
        kolTestColumns: [
            {
                title: 'Name',
                dataIndex: 'name',
                key: 'name',
                width: 120,
            },
            {
                title: 'Value',
                dataIndex: 'value',
                key: 'value',
            },
        ],
        kolDetail:{
            key:1,
            user: '扇子NO_FAN_NO_FUN',
            testData: [
                {
                    key: '1',
                    name: 'KILL分数',
                    value: '45'

                },
                {
                    key: '2',
                    name: '粉丝数',
                    value: '1198257',
                },
                {
                    key: '3',
                    name: '有效粉丝率',
                    value: '60.2%'                ,
                },
                {
                    key:'4',
                    name:'有效粉丝量',
                    value:'103002'
                },
                {
                    key: '5',
                    name: '关注数',
                    value: '785'
                },
                {
                    key: '6',
                    name: '日常曝光',
                    value: '785'
                },
                {
                    key: '7',
                    name: '日常互动',
                    value: '7785',
                },
                {
                    key: '8',
                    name: '评论',
                    value: '3431'                ,
                },
                {
                    key:'9',
                    name:'转发',
                    value:'1193'
                },
                {   key: '10',
                    name:'点赞',
                    value:'1193'
                },
                {
                    key: '11',
                    name: '传播层级',
                    value: '0.9'

                },
                {
                    key: '12',
                    name: '正面情感',
                    value: '68.17%',
                },
                {
                    key: '13',
                    name: '曝光质量度',
                    value: '8.47%',
                },
                {
                    key: '14',
                    name: '互动质量度',
                    value: '6.02%'                ,
                },
                {
                    key:'15',
                    name:'评论质量度',
                    value:'10.77%'
                },
                {
                    key: '16',
                    name: '转发质量度',
                    value: '6.02%'                ,
                },
                {
                    key:'17',
                    name:'点赞质量度',
                    value:'10.77%'
                },
                {
                    key:'18',
                    name:'参与度',
                    value:'10.77%'
                }
            ],
            testDataAfter: [
                {
                    key: '1',
                    name: 'KILL分数',
                    value: '58'

                },
                {
                    key:'2',
                    name:'发表时间',
                    value:'2018/11/21  13:25:01'
                },
                {
                    key: '3',
                    name: '粉丝数',
                    value: '1198257',
                },
                {   key: '4',
                    name:'有效粉丝率',
                    value:'20.45%'
                },
                {
                    key:'5',
                    name:'有效粉丝量',
                    value:'103002'
                },
                {
                    key: '6',
                    name: '曝光',
                    value: '843276'

                },
                {
                    key: '7',
                    name: '互动',
                    value: '889',
                },
                {
                    key: '8',
                    name: '参与度',
                    value: '0.11%'
                },
                {
                    key: '9',
                    name: '传播层级',
                    value: '1'                ,
                },
                {
                    key:'10',
                    name:'正面情感',
                    value:'60%'
                },
                {
                    key: '11',
                    name: '曝光质量度',
                    value: '20.85%'

                },
                {
                    key: '12',
                    name: '互动质量度',
                    value: '68.17%',
                },
                {
                    key: '13',
                    name: '评论质量度',
                    value: '68.17%',
                },
                {
                    key: '14',
                    name: '转发质量度',
                    value: '8.47%'
                },
                {
                    key: '15',
                    name: '点赞质量度',
                    value: '6.02%'                ,
                },
                {
                    key:'16',
                    name:'品牌提及',
                    value:'10.77%'
                },
                {
                    key: '17',
                    name: '评论',
                    value: '63'                ,
                },
                {
                    key:'18',
                    name:'转发',
                    value:'174'
                },
                {
                    key:'19',
                    name:'点赞',
                    value:'222'
                },
                {
                    key: '20',
                    name: '内容Link',
                    value:  'http://weibo.com/1652466293/GvEGgDw0X'
                }
            ],
            thirdPartyTestData: [
                {
                    key: '1',
                    number: '2018081100',
                    name: '1115 power pack',
                    platform: '微博',
                    content: '品牌曝光',
                    qianCe: '45',
                    houCe: '58',
                    readerShip: '1,261,747',
                    engagement: '3,431',
                    cost: '1,000,0',
                    roi: '0.91',
                    cpc: '--',
                    cpr: '--',
                    start: 5,
                },
                {
                    key: '2',
                    number: '2018081101',
                    name: '1115 selling kit',
                    platform: '微博',
                    content: '种草',
                    qianCe: '33',
                    houCe: '32',
                    readerShip: '1,221,684',
                    engagement: '3,373',
                    cost: '1,000,0',
                    roi: '0.91',
                    cpc: '--',
                    cpr: '--',
                    start: 5,
                },
                {
                    key: '3',
                    number: '2018081102',
                    name: '1018 OBEB Program',
                    platform: '微博',
                    content: '带货',
                    qianCe: '36',
                    houCe: '32',
                    readerShip: '1,773,114',
                    engagement: '8,052',
                    cost: '1,000,0',
                    roi: '0.91',
                    cpc: '--',
                    cpr: '--',
                    start: 5,
                },
                {
                    key: '4',
                    number: '2018081103',
                    name: '0920 全球打卡',
                    platform: '微博',
                    content: '带货',
                    qianCe: '36',
                    houCe: '34',
                    readerShip: '1,624,515',
                    engagement: '759',
                    cost: '1,000,0',
                    roi: '0.91',
                    cpc: '--',
                    cpr: '--',
                    start: 3,
                },
                {
                    key: '5',
                    number: '2018081104',
                    name: '0525 美妆节',
                    platform: '微博',
                    content: '种草',
                    qianCe: '36',
                    houCe: '38',
                    readerShip: '1,245,747',
                    engagement: '962',
                    cost: '1,000,0',
                    roi: '0.91',
                    cpc: '--',
                    cpr: '--',
                    start: 3,
                },
                {
                    key: '6',
                    number: '2018081105',
                    name: '0322 TBFF',
                    platform: '微博',
                    content: '品牌曝光',
                    qianCe: '29',
                    houCe: '29',
                    readerShip: '1,054,265',
                    engagement: '1,054',
                    cost: '1,000,0',
                    roi: '0.91',
                    cpc: '--',
                    cpr: '--',
                    start: 1,
                },
            ],
            bankInfomation:{
                name:'邓卓翔',
                bankCardAccountName:'邓卓翔',
                bankName:'中国银行',
                bankAccountOpeningBank:'中国银行上海静安区支行',
                bankCardNumber:'1234123412341234',
                contactNumber:'15622100591',
                idCard:'445103199621367458',
                icCardPhoto:'http://obuat.watsonsestore.com.cn/pss/kol_databases/kol01.jpg'
            },
            kolInfomation:{
                name:'邓卓翔',
                phone:'13725826542',
                email:'3412@qq.com',
                address:'上海静安区',
                qq:'3412',
                weChat:'WeChatWeChat',
                fileList: [
                    {
                        uid: '-1',
                        name: 'xxx.png',
                        status: 'done',
                        url: 'http://obuat.watsonsestore.com.cn/pss/kol_databases/kol01.jpg',
                    },
                    ],
            },
            kolPlatformInfomation:{
                mediaPlatform:'微博',
                label:'时尚',
                id:'于艾希Icey',
                numberOfFans:'1161271',
                averageReading:'32326',
                averageInter:'55245',
                location:'海外 其他',
                cooperationBrand:'posts,beats,cocah',
                averageInteraction:' 2,030',
                area:'华东',
                contact:'Danny',
                auditNote:'--',
                remarks:'备注'
            }
        }
    },
};
