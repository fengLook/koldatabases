import React, { Component } from 'react';
import { Layout, Menu, Icon, Button, Form, Input, Dropdown, Row, Col, Avatar } from 'antd';
import Link from 'umi/link';
import IndexHeader from './header.js';

const { Footer, Sider, Content } = Layout;

const SubMenu = Menu.SubMenu;// 引入子菜单组件

export default class BasicLayout extends Component {
  render() {
    return (
      <Layout>
                    <IndexHeader name='JJ DENG'></IndexHeader>
                    <Layout>
                      <Sider
                        width={256}
                        style={{ minHeight: 'calc(100vh - 64px)',background: '#fff' }}
                      >
                        <Menu
                          mode="inline"
                          defaultSelectedKeys={['1']}
                          style={{ height: 'calc(100vh - 64px)' }}
                        >
                           <Menu.Item key="1"><Link to="/"><Icon type="stock" /><span>数据分析</span></Link></Menu.Item>
                          <Menu.Item key="2">
                            <Link to="/kol">
                              <Icon type="solution" />
                              <span>Kol 信息</span>
                            </Link>
                          </Menu.Item>
                          <Menu.Item key="3">
                            <Link to="/project">
                              <Icon type="gateway" />
                              <span>项目</span>
                            </Link>
                          </Menu.Item>
                            <Menu.Item key="4">
                                <Link to="/topKolPool">
                                    <Icon type="bar-chart" />
                                    <span>Top Kol Pool</span>
                                </Link>
                            </Menu.Item>
                            <SubMenu
                                key="sub1"
                                title={<span><Icon type="dashboard" /><span>系统配置</span></span>}
                            >
                                <Menu.Item key="10">
                                    <Link to="/system_config">
                                        <Icon type="setting" />
                                        <span>报名页面配置</span>
                                    </Link>
                                </Menu.Item>
                                <Menu.Item key="11">
                                    <Link to="/platform">
                                        <Icon type="ellipsis" />
                                        <span>平台信息</span>
                                    </Link>
                                </Menu.Item>
                                <Menu.Item key="12">
                                    <Link to="/bank">
                                        <Icon type="property-safety" />
                                        <span>银行</span>
                                    </Link>
                                </Menu.Item>
                                <Menu.Item key="13">
                                    <Link to="/tags">
                                        <Icon type="tag" />
                                        <span>标签</span>
                                    </Link>
                                </Menu.Item>
                                <Menu.Item key="14">
                                    <Link to="/region_code">
                                        <Icon type="qrcode" />
                                        <span>区域二维码</span>
                                    </Link>
                                </Menu.Item>
                            </SubMenu>
                        </Menu>
                      </Sider>
                      <Content style={{ margin: '24px 16px 0' }}>
                        <div style={{ padding: 24, background: '#fff', minHeight: 360 }}>
                          {this.props.children}
                        </div>
                      </Content>
                    </Layout>
      </Layout>
    );
  }
}
