export default [
  // user
  {
    path: '/user',
    component: '../layout/UserLayout',
    routes: [
      { path: '/user', redirect: '/user/login' },
      { path: '/user/login', component: './User/Login' },
      // { path: '/user/register', component: './User/Register' },
      // { path: '/user/register-result', component: './User/RegisterResult' },
    ],
  },
  // app
  {
    path: '/',
    component: '../layout',
    Routes: ['src/pages/Authorized'],
    authority: ['admin', 'user'],
    routes: [
      // dashboard
      { path: '/', redirect: '/dashboard' },
      {
        path: '/dashboard',
        name: 'dashboard',
        icon: 'dashboard',
        routes: [
            {
                path: '/dashboard',
                name: 'dashboard',
                component: './Dashboard',
            },
            {
                path: 'kol',
                routes: [
                    {
                        path: '/kol',
                        authority: ['admin'],//配置准入权限,可以配置多个角色
                        component: 'Kol'
                    },
                    { path: '/kol/detail/:page', hideInMenu: true, name: 'detail', component: 'KolDetail' },
                ],
            },
            {
                path: 'old_kol',
                component: './OldKol',
            },
            {
                path: 'Platform',
                component: './Platform',
            },
            {
                path: 'system_config',
                component: './SystemConfig',
            },
            {
                path: 'region_code',
                component: './RegionCode',
            },
            {
                path: 'project',
                routes: [
                    { path: '/project', component: './Project' },
                    { path: '/project/plan/:page', hideInMenu: true, name: 'plan', component: './Plan' },
                    { path: '/project/planDetail/:page', hideInMenu: true, name: 'planDetail', component: './PlanDetail' },
                ],
            },
            {
                path: 'bank',
                routes: [
                    { path: '/bank', component: 'Bank' },
                    { path: '/bank/detail/:page', hideInMenu: true, name: 'detail', component: 'BankDetail' },
                ],
            },
            {
                path: 'tags',
                component: './Tags',
            },
            {
                path: 'topKolPool',
                component: './TopKolPool',
            },
         ]
      },
      // {
      //   component: '404',
      // },
    ],
  },
];
