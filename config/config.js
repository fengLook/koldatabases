
import pageRoutes from './router.config';
export default {
  plugins: [
    [
      'umi-plugin-react',
      {
        antd: true,
        dva: true,
      },
    ],
  ],
    history: 'hash', //采用hash路由：#/xxx的形式
    base:'./',
    publicPath:'./',
    // 路由配置
    routes: pageRoutes,
    singular: true,
    proxy: {
       '/dev': {
             // target: 'https://08ad1pao69.execute-api.us-east-1.amazonaws.com',
                  target:'https://wcmall.watsons.com.cn',
                  changeOrigin: true,
              },
    },
    // proxy: {
    //     '/api/kol/': {
    //         target: 'http://10.82.33.118:3001/parse/',
    //         changeOrigin: true,
    //     },
    // },
};
